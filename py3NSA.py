import h5py
import xmltodict
import sys
import numpy as np
import pdb
import datetime
import os
import fnmatch
import time
import ephem
#from matplotlib.mlab import find
from scipy import interpolate
from scipy.signal import filtfilt
from scipy.signal import lfilter
from scipy.signal import lfilter_zi 
from scipy.signal import lfiltic 
from scipy.signal import medfilt
from scipy.optimize import curve_fit
from scipy.optimize import lsq_linear
from scipy.interpolate import LSQUnivariateSpline
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy import sparse
from scipy.sparse.linalg import spsolve
from pybaselines import Baseline
from matplotlib.pyplot import *
import json
import urllib
import requests
import healpy
import pygdsm as pygsm
import pudb
import glob
ion()


############# Python functions library for NSA data analysis ################
#
# Copyright 2020 Benjamin Censier
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

def find(condition):
    """Returns indices where ravel(a) is true.
    Private implementation of deprecated matplotlib.mlab.find
    """
    return np.nonzero(np.ravel(condition))[0]



class MetaArray(np.ndarray):
            """
            Array with metadata.
            Ref: https://stackoverflow.com/questions/34967273/add-metadata-comment-to-numpy-ndarray
            """

            def __new__(cls, array, dtype=None, order=None, **kwargs):
                    obj = np.asarray(array, dtype=dtype, order=order).view(cls)
                    obj.metadata = kwargs
                    return obj

            def __array_finalize__(self, obj):
                    if obj is None: return
                    self.metadata = getattr(obj, 'metadata', None)


#meta_dt = np.dtype(['pouet',MetaArray])                    

def read_NSA_hdf5(filename,meta_only=False,stdout=True, cal=False,stat_output=False):
            """
            Read NSA hdf5 file in path 'filename', metadata (attributes) only if meta_only = True
            Outputs:
                    metadata : dictionnary with metadata
                    DATA : spectrum data in a dictionary, one key for each acqrec, for each acqrec a numpy array of size npointings x npolar x nspectres x nchannel
                           NB: this is a numpy array with .metadata field containing the path of the file the data were read in.
                    TIMESTAMP : data timestamp in a dictionary, one key for each acqrec, for each acqrec a numpy array of size npointings x npolar x nspectres
                    AZIMUTH : AZIMUTH in a dictionary, one key for each acqrec, for each acqrec a numpy array of size npointings x npolar x nspectres

            """
            if cal:
                data_str = "Cal"
            else:
                data_str = "Acq"
            f=h5py.File(filename,'r')
            
################################ Read Metadata (attributes) #####################################

            Nacqrec = len(f.keys())
            #pdb.set_trace()
            if type(f.attrs['ObsStart']) == str:
                try:
                        ObsStart = datetime.datetime.strptime(f.attrs['ObsStart'],"%Y-%m-%dT%H:%M:%S.%fZ")
                except:
                        ObsStart = datetime.datetime.strptime(f.attrs['ObsStart'],"%Y-%m-%dT%H:%M:%SZ")
                try:
                        ObsStop = datetime.datetime.strptime(f.attrs['ObsStop'],"%Y-%m-%dT%H:%M:%S.%fZ")
                except:
                        ObsStop = datetime.datetime.strptime(f.attrs['ObsStop'],"%Y-%m-%dT%H:%M:%SZ")
            else: # special case sidereal time in radians
                ObsStart = f.attrs['ObsStart']
                ObsStop = f.attrs['ObsStop']
                
            ObsIdCfg = f.attrs['ObsIdCfg']
            #pdb.set_trace()
            AntennaCoordinates = f.attrs['AntennaCoordinates'].split('+')[1:]
            i = 0
            for a in AntennaCoordinates:
                        k=0
                        while a[k] == '0': # remove leading 0 in string
                                    
                                    k+=1
                        AntennaCoordinates[i] = a[k:].split('.')[0]
                        AntennaCoordinates[i] = float(AntennaCoordinates[i][-2:])/3600.+float(AntennaCoordinates[i][-4:-2])/60.+float(AntennaCoordinates[i][:-4])
                        i+=1

#            AntennaCoordinates = [float(a[:2])+float(a[2:4])/60.+float(a[4:6])/3600. for a in AntennaCoordinates]
            AntennaCoordinates.reverse() # lon, lat
            try:            
                 AntennaHeight = float(f.attrs['AntennaHeight'])
            except:
                 AntennaHeight = np.nan

            AntennaType = f.attrs['AntennaType']

            Fcenter = np.zeros(Nacqrec)
            ScanDuration = np.zeros(Nacqrec)
            SweepNum = np.zeros(Nacqrec,dtype=int)
            BinNum = np.zeros(Nacqrec,dtype=int)
            PointingNum = np.zeros(Nacqrec,dtype=int)
            ChanInteTime = np.zeros(Nacqrec)
            Span = np.zeros(Nacqrec)
            RBW = np.zeros(Nacqrec)
            #VBW = np.zeros(Nacqrec) string
            NumScan = np.zeros(Nacqrec,dtype=int)
            if stat_output:
                NumScan_stat = np.zeros(Nacqrec,dtype=int)
            Fa3 = np.zeros(Nacqrec)
            Fb3 = np.zeros(Nacqrec)                    
            
            i=0
            for key,value in f.items():
                    #print key, value
                    Fcenter[i] = f[key].attrs['Fcenter']
                    ScanDuration[i] = f[key].attrs['ScanDuration']
                    SweepNum[i] = int(f[key].attrs['SweepNum'])
                    BinNum[i] = int(f[key].attrs['BinNum'])
                    Span[i] = f[key].attrs['Span']
                    RBW[i] = f[key].attrs['RBW']
                    #VBW[i] = f[key].attrs['VBW']
                    Fa3[i] = f[key].attrs['Fa3']
                    Fb3[i] = f[key].attrs['Fb3']                    
                    NumScan[i] = int(f[key]['Pointing_000']['Polar_0'][data_str].attrs['NumScan']) #*2 # factor 2 because two polarizations !!!!!!! SUPPOSE SAME NUMSCAN FOR ALL POINTINGs
                    if stat_output:
                        NumScan_stat[i] = int(f[key]['Pointing_000']['Polar_0']['Stat'].attrs['NumScan']) #*2 # factor 2 because two polarizations !!!!!!! SUPPOSE SAME NUMSCAN FOR ALL POINTINGs
                    PointingNum[i] = len(f[key].keys()) 
                    ChanInteTime[i] = f[key]['Pointing_000']['Polar_0'][data_str].attrs['ChanInteTime']
                    i+=1

            Tint = SweepNum*ScanDuration # integration time (s)
            
            Freqs = {} # frequency axis in a dictionnary
            for i in range(Nacqrec):
                    Freqs[str(i)] = np.linspace(Fcenter[i]-Span[i]/2,Fcenter[i]+Span[i]/2,BinNum[i]) 


            # auto output of every attributes
            #for acqrec_id,acqrec in f.items():
            #	for key,value in f[acqrec_id].attrs.items():
            #		if type(value) != unicode:
            #			eval(compile(key+'_'+acqrec_id+'='+str(value),'<string>','exec'))

            Nf = BinNum.copy() # nb of frequency bins
            Np = PointingNum.copy() # nb of pointings
            #len(f[f.keys()[0]][f[f.keys()[0]].keys()[0]]) # nb of pointings
#f['/AcqRec_006/Pointing_000/Polar_0/Acq'].attrs['ChanInteTime']
            metadata = {}

            metadata['Nacqrec'] = Nacqrec
            metadata['PointingNum'] = PointingNum
            metadata['Fcenter'] = Fcenter
            metadata['ScanDuration'] = ScanDuration
            metadata['SweepNum'] = SweepNum        
            metadata['BinNum'] = BinNum
            metadata['Span'] = Span
            metadata['RBW'] = RBW                
            metadata['NumScan'] = NumScan
            if stat_output:
                metadata['NumScan_stat'] = NumScan_stat
            metadata['ChanInteTime'] = ChanInteTime            
            metadata['Tint'] = Tint
            metadata['Freqs'] = Freqs
            metadata['ObsStop'] = ObsStop
            metadata['ObsStart'] = ObsStart            
            metadata['ObsIdCfg'] = ObsIdCfg
            metadata['filename'] = filename
            metadata['AntennaCoordinates'] = AntennaCoordinates
            metadata['ObsIdCfg'] = ObsIdCfg
            metadata['AntennaHeight'] = AntennaHeight
            metadata['AntennaType'] = AntennaType
            metadata['Fa3'] = Fa3
            metadata['Fb3'] = Fb3

################################ Read Data #####################################

            if not meta_only:

                    DATA = {}
                    STAT = {}
                    TIMESTAMP = {}
                    AZIMUTH = {}
                    iFILE = {}
                    for i in range(Nacqrec):
                            DATA[str(i)] = MetaArray(np.zeros((Np[i],2,NumScan[i],Nf[i])),filename = filename) #np.zeros((Np[i],2,NumScan[i],Nf[i])) #npointings x npolar x nspectres x nchannel
                            if stat_output:
                                STAT[str(i)] = np.zeros((Np[i],2,NumScan_stat[i],Nf[i])) #np.zeros((Np[i],2,NumScan[i],Nf[i])) #npointings x npolar x nspectres x nchannel                            
                            if type(f.attrs['ObsStart']) != str:
                                 TIMESTAMP[str(i)] = np.zeros((Np[i],2,NumScan[i])) # time stamps for every acq / sidereal time in radians case
                            else:
                                 TIMESTAMP[str(i)] = np.zeros((Np[i],2,NumScan[i]),dtype='datetime64[us]') # time stamps for every acq
                                 
                            AZIMUTH[str(i)] = np.zeros((Np[i],2,NumScan[i]))
                            iFILE[str(i)] = np.zeros((Np[i],2,NumScan[i])) # to store id of file in the scan list

                    i_acqrec = 0
                    i_pointing = 0
                    i_polar = 0
                    i_acq=0
                    i_acq_stat=0
                    i_acq_point_polar = 0

                    if stdout:
                            Ndash = 40
                            print("> Reading data...")
                            sys.stdout.write(">"+"-"*Ndash+" [0%]\r")
                    t0 = time.time()
                    
                    for acqrec_id, acqrec in f.items(): # acqrec_id = 0..Nacq-1
                            for key_pointing in acqrec.keys():
                                    for key_polar in acqrec[key_pointing].keys():

                                            if stat_output: # output statistics

                                                for key_acq in acqrec[key_pointing][key_polar]['Stat'].keys():
                                                    if i_acq_stat < NumScan_stat[i_acqrec]:
                                                        STAT[str(i_acqrec)][i_pointing,i_polar,i_acq_stat,:] = acqrec[key_pointing][key_polar]['Stat'][key_acq]
                                                        i_acq_stat+=1



                                                
                                            for key_acq in acqrec[key_pointing][key_polar][data_str].keys():
                                                    #print(acqrec[key_pointing][key_polar][data_str][key_acq].shape)
                                                    DATA[str(i_acqrec)][i_pointing,i_polar,i_acq,:] = acqrec[key_pointing][key_polar][data_str][key_acq]
                                                    timestamp_temp = acqrec[key_pointing][key_polar][data_str][key_acq].attrs['TimeStamp']
                                                    if type(timestamp_temp) == str:
                                                         
                                                         TIMESTAMP[str(i_acqrec)][i_pointing,i_polar,i_acq] = datetime.datetime.strptime(timestamp_temp.split('Z')[0],"%Y-%m-%dT%H:%M:%S.%f")
                                                    else:
                                                        TIMESTAMP[str(i_acqrec)][i_pointing,i_polar,i_acq] = timestamp_temp # sidereal time case
                                                    AZIMUTH[str(i_acqrec)][i_pointing,i_polar,i_acq] = acqrec[key_pointing].attrs['Azimuth']
                                                    #iFILE[str(i_acqrec)][i_pointing,i_polar,i_acq] = ifile
                                                    i_acq+=1
                                            i_acq = 0
                                            i_polar+=1
                                            i_acq_point_polar+=1
                                            if stdout:
                                                    frac_done = float(i_acq_point_polar)/(sum(Np*2))
                                                    Ndone = int(np.round(Ndash*frac_done))
                                                    sys.stdout.flush()
                                                    sys.stdout.write("|"*Ndone+">"+"-"*(Ndash-Ndone-1)+" ["+"%d"%(int(frac_done*100))+"%]\r")
                                                                                
                                    i_polar = 0
                                    i_pointing+=1
                            i_pointing = 0
                            i_acqrec+=1

                    t1 = time.time()
                    
                    if stdout:
                            print("\nDone. ("+"%.2f"%(t1-t0)+"s)")

                    #f.close()
                    f.close()
                    if stat_output:
                        return metadata, DATA, TIMESTAMP, AZIMUTH, STAT
                    else:
                        return metadata, DATA, TIMESTAMP, AZIMUTH


            else:
                    f.close()
                    return metadata


################################### end read_NSA_hdf5() #############################


def ana_hdf5(filename,Fmin,Fmax,method="drpls",plotfig=False):
    """
    Every data analysis that has to be done on a single hdf5 file, with monitoring in mind (autmatic tasks on regular observations).
    - baseline model : baseline fitting vs. frequency on an average/median spectrum & baseline removing from 2D time/frequency plan for each time integration
    - noise model : MAD measurement in each freq channel not excluded by 1s step (outliers==RFI lines)
    - thresholding : time/frequency binary array, 1 where above threshold, O elsewhere
    - average occupancy in t/f plan based on thresholding : on the whole acquisition (single number, one for each time integration) / at a given dt/df resolution / max occupancy ...)

    Inputs:
    filename : path of hdf5 file to be analyzed (string) OR dictionnary with metadata, DATA, TIMESTAMP and AZIMUTH keys (read the provided dic instead of the file)
    Fmin, Fmax : min/max frequency for analysis (Hz)
    method : method used for baseline determination : 
             "spline" : homemade recursive spline fitting with outliers discarding (good but slow and sometimes unstable)
             "drpls" [default] : Doubly Reweighted Penalized Least Squares (pybaselines module, https://pybaselines.readthedocs.io/en/latest/algorithms/whittaker.html?highlight=drpls#drpls-doubly-reweighted-penalized-least-squares)
             "airpls" : Adaptive Iteratively Reweighted Penalized Least Squares (pybaselines, see link above)
             "derpsalsa" : Derivative Peak-Screening Asymmetric Least Squares Algorithm (pybaselines, see link above)

    Output: dictionnary with lots of stuff
    """

    
    if type(filename) == str:
        metadata, DATA, TIMESTAMP, AZIMUTH = read_NSA_hdf5(filename,meta_only=False,stdout=True, cal=False)
    else: # input is a dictionnary with metadata, DATA, TIMESTAMP and AZIMUTH keys
        metadata = filename['metadata'].copy()
        DATA = filename['DATA'].copy()
        TIMESTAMP = filename['TIMESTAMP'].copy()
        AZIMUTH = filename['AZIMUTH'].copy()

    if type(Fmin) != list: # if not list, make it a list
        Fmin = [Fmin]*len(DATA.keys())
    if type(Fmax) != list: # if not list, make it a list
        Fmax = [Fmax]*len(DATA.keys())


        # init
#    DATA_corr = dict(DATA) #.copy() # data dic with baseline removed # caused troubles !!
    DATA_corr = {}
    for key in DATA.keys():
        DATA_corr[key] = np.zeros(DATA[key].shape)
#    DATA_thresh = dict(DATA) #.copy() # data dic with 1 where above threshold, 0 elsewhere
    DATA_thresh = {}
    for key in DATA.keys():
        DATA_thresh[key] = np.zeros(DATA[key].shape)
    MAD = {} # MAD of each freq channel
    thresh_K = {} # threshold in K
    baseline = {}
    ikey = 0
    for key in metadata['Freqs'].keys():

        i1 = np.argmin(np.abs(metadata['Freqs'][key]-Fmin[ikey]))
        i2 = np.argmin(np.abs(metadata['Freqs'][key]-Fmax[ikey]))
        #imaxnoise = np.argmin(np.abs(metadata['Freqs'][key]-30e6))
        ikey+=1


        if method == "spline":
            err = True
            n_knots_list = range(20,3,-1) #[13,12,11,14,10,15,9]
            i = 0

            while err == True and i<len(n_knots_list):
                try:
    #                pudb.set_trace()
                    baseline_PX,spl_PX, iimax_X, max_list_X = fit_spectrum2(metadata['Freqs'][key][i1:i2], np.median(DATA[key][0,0,:,i1:i2].T,axis=1), n=11, n_knots=n_knots_list[i],max_output=True,plotfig=plotfig)
                    err = False
                except:
                    i+=1
            if err == True:
                print("##### Spectrum spline fit problem Polar 0 #############")
                def spl_PX(x):
                    return np.nan*np.zeros(len(metadata['Freqs'][key]))
    #        baseline_PY,spl_PY, iimax_Y, max_list_Y = py3NSA.fit_spectrum2(metadata['Freqs'][key][i1:i2], np.median(DATA[key][0,1,:,i1:i2].T,axis=1), n=11, n_knots=13,max_output=True) # 2ème polar


            err = True
            n_knots_list = range(20,3,-1) #[13,12,11,14,10,15,9]
            i = 0

            while err == True and i<len(n_knots_list):
                try:
    #                pudb.set_trace()
                    baseline_PY,spl_PY, iimax_Y, max_list_Y = fit_spectrum2(metadata['Freqs'][key][i1:i2], np.median(DATA[key][0,1,:,i1:i2].T,axis=1), n=11, n_knots=n_knots_list[i],max_output=True,plotfig=plotfig)
                    err = False
                except:
                    i+=1
            if err == True:
                print("##### Spectrum spline fit problem Polar 1 #############")
                def spl_PY(x):
                    return np.nan*np.zeros(len(metadata['Freqs'][key]))





            # substract baseline
            baseline[key] = np.zeros((2,len(spl_PX(metadata['Freqs'][key]))))
            baseline[key][0,:] = spl_PX(metadata['Freqs'][key])
            baseline[key][1,:] = spl_PY(metadata['Freqs'][key])


        elif method != "spline":
            baseline[key] = np.zeros((2,len(metadata['Freqs'][key])))
            baseline[key][0,i1:i2] = 10**(baseline_pybaselines(metadata['Freqs'][key][i1:i2], 10*np.log10(np.nanmedian(DATA[key][0,0,:,i1:i2].T,axis=1)),method=method)/10)
            baseline[key][1,i1:i2] = 10**(baseline_pybaselines(metadata['Freqs'][key][i1:i2], 10*np.log10(np.nanmedian(DATA[key][0,1,:,i1:i2].T,axis=1)),method=method)/10)
            if plotfig:
                figure()
                plot(metadata['Freqs'][key],10*np.log10(np.median(DATA[key][0,0,:,:].T,axis=1)))
                plot(metadata['Freqs'][key][i1:i2],10*np.log10(baseline[key][0,i1:i2]),'r--',label='baseline')


        baseline[key][0,i2:] = np.nan
        baseline[key][0,:i1] = np.nan
        baseline[key][1,i2:] = np.nan
        baseline[key][1,:i1] = np.nan
        
        dT = metadata['Tint'][int(key)] # integration time
        dF = metadata['Freqs'][key][1]-metadata['Freqs'][key][0]
        k_thresh = np.sqrt(2*np.log(len(metadata['Freqs'][key][i1:i2]))) # for universal threshold (see Donoho et al.)
#        thresh = k_thresh/np.sqrt(dF*dT) + 0.1 # threshold with a 0.1 margin to account for excess noise
        DATA_corr[key][0,0,:,:] = DATA[key][0,0,:,:].copy()/np.tile(baseline[key][0,:],(DATA[key][0,0,:,:].shape[0],1))
        DATA_corr[key][0,1,:,:] = DATA[key][0,1,:,:].copy()/np.tile(baseline[key][1,:],(DATA[key][0,1,:,:].shape[0],1))
        
        thresh1 = 1 + k_thresh/np.sqrt(dF*dT) #+ 0.05# threshold with a 0.1 margin to account for excess noise
        thresh2 = 1 + k_thresh*stdMAD_recurs(DATA_corr[key][0,0,0,i1:i2])
        thresh = max(thresh1,thresh2)
        thresh_K[key] = np.zeros(baseline[key].shape)
        thresh_K[key][0,:] = thresh*baseline[key][0,:] # threshold in K

        thresh2 = 1 + k_thresh*stdMAD_recurs(DATA_corr[key][0,1,0,i1:i2])
        thresh = max(thresh1,thresh2)
        thresh_K[key][1,:] = thresh*baseline[key][1,:] # threshold in K        


        # figure()
        # plot(metadata['Freqs'][key][i1:i2],10*np.log10(DATA[key][0,0,:,i1:i2].T)) 
        # plot(metadata['Freqs'][key][i1:i2],10*np.log10(thresh_K[key][i1:i2]),'k--')
        
        ii_polar0 = find(np.squeeze(DATA_corr[key][0,0,:,:])>thresh)
        ii_polar1 = find(np.squeeze(DATA_corr[key][0,1,:,:])>thresh)
        # if key == '5' or key == '6':
        #     pudb.set_trace()
            
        DATA_thresh[key] = np.zeros(DATA[key].shape)
        if len(np.squeeze(DATA_corr[key][0,0,:,:]).shape) == 2:
            u,v = np.unravel_index(ii_polar0,np.squeeze(DATA_corr[key][0,0,:,:]).shape)
            DATA_thresh[key][0,0,u,v] = 1
        else:
            v = np.unravel_index(ii_polar0,np.squeeze(DATA_corr[key][0,0,:,:]).shape)
            DATA_thresh[key][0,0,0,v] = 1

        if len(np.squeeze(DATA_corr[key][0,1,:,:]).shape) == 2:
            u,v = np.unravel_index(ii_polar1,np.squeeze(DATA_corr[key][0,1,:,:]).shape)
            DATA_thresh[key][0,1,u,v] = 1
        else:
            v = np.unravel_index(ii_polar1,np.squeeze(DATA_corr[key][0,1,:,:]).shape)
            DATA_thresh[key][0,1,0,v] = 1
            
        # figure()
        # imshow(DATA_thresh[key][0,0,:,:].T,interpolation='None')
        # colorbar()
        # print('> Plotting...')
        # figure()
        # plot(metadata['Freqs'][key],DATA_corr[key][0,0,:,:].T)
        # plot(metadata['Freqs'][key],np.mean(DATA_corr[key][0,0,:,:],axis=0).T,'k--',linewidth=2,alpha=0.5)
        # plot(metadata['Freqs'][key],thresh*np.ones(len(metadata['Freqs'][key])),'k--')
        # plot(metadata['Freqs'][key][v],DATA_corr[key][0,0,u,v].T,'r.')
        
#        compute MAD for each freq channel
        MAD[key] = np.zeros((2,DATA_corr[key][0,0,:,:].shape[1]))
        for i in range(DATA_corr[key][0,0,:,:].shape[1]):
            MAD[key][0,i] = stdMAD_recurs(DATA_corr[key][0,0,:,i])
            MAD[key][1,i] = stdMAD_recurs(DATA_corr[key][0,1,:,i])
            
        OUT = {}
        OUT['DATA'] = DATA.copy()
        OUT['DATA_corr'] = DATA_corr.copy()
        OUT['DATA_thresh'] = DATA_thresh.copy()
        OUT['MAD'] = MAD.copy()
        OUT['baseline'] = baseline.copy()
        OUT['thresh_K'] = thresh_K.copy()
        OUT['k_thresh'] = k_thresh.copy()
        OUT['metadata'] = metadata
        OUT['TIMESTAMP'] = TIMESTAMP
        OUT['AZIMUTH'] = AZIMUTH
            


    return OUT


# def scan_obs(META,Fmin,Fmax):
#     """
#     scan observations listed in the metadata dic provided META, computing for each of them:
#     - baseline spectrum
#     - noise in each subband
#     - threshold in each subband
#     - thresholded data (1 above threshold, 0 elsewhere)
#     - raw data
#     - corrected data (data - baseline)
#     See ana_hdf5()

#     Based on ana_hdf5 output, the median, max, min and occupancy spectra are computed (+ snapshot of time/frequency in png file ?)
#     """

#     for filename in META['filename']:

#         TOC = {} # occupancy in each subband
#         MAX = {} # max spectrum
#         MIN = {} # min spectrum
#         MED = {} # median spectrum
#         OUT_temp = ana_hdf5(filename,Fmin,Fmax)
#         metadata = OUT_temp['metadata']
#         TINT = list(set(np.concatenate(META['Tint'])))

#         for dt in metadata['Tint']:
                        
#             k = TINT.index(dt)
#         for key in OUT_temp['DATA'].keys():
#             DATA = OUT_temp['DATA'][key][0,0,:,:]
#             if TOC == {}:
#                 TOC[key] = []
                
#             TOC[key] = np.nansum(DATA,axis=0)/DATA.shape[0]
#             MAX[key] = np.nanmax(DATA,axis=0)
#             MIN[key] = np.nanmin(DATA,axis=0)
#             MED[key] = np.nanmedian(DATA,axis=0)


            
#         # obsstart, obsstop, baseline, max/min/med in a t/f array with day resolution

        
        
def write_cal_hdf5(timestamp,data,filename,type_data=0,unit_str='K'):
        """
        Write data (dictionnary of numpy arrays, one numpy array for each acqrec/integration time of dimension NpointingxNpolarxNacqxNtxNf) in a hdf5 file following NSA RMHDF structure. HDF5 attributes are defined in the "metadata" dictionnary (+ timestamp given as datetime)
        """


        f=h5py.File(filename,'w')
         
        # i = 0
        # for key,value in metadata.items():
        #     try:
        #         len(value)
        #         if len(value)>1 and type(value) != str:
        #              exec(key+" = metadata[\'"+key+"\'][0]",globals(),locals())


        #              print(key+" = metadata[\'"+key+"\'][0]")                     
                     
        #         else:
        #              exec(key+" = metadata[\'"+key+"\']",globals(),locals())
        #              print(key+" = metadata[\'"+key+"\']")                     
        #     except:
        #         exec(key+" = metadata[\'"+key+"\']",globals(),locals())
        #         print(key+" = metadata[\'"+key+"\']")                
        #     i+=1



        # Nacqrec = metadata['Nacqrec']
        # PointingNum = metadata['PointingNum'][0]
        # Fcenter = metadata['Fcenter'][0]
        # ScanDuration = metadata['ScanDuration'][0]
        # SweepNum = metadata['SweepNum'][0]
        # BinNum = metadata['BinNum'][0]
        # Span = metadata['Span'][0]
        # RBW = metadata['RBW'][0]
        # NumScan = data.shape[1] #metadata['NumScan'][0]
        # ChanInteTime = metadata['ChanInteTime'][0]
        # Tint = metadata['Tint'][0]
        # Freqs = metadata['Freqs']
        # ObsStop = metadata['ObsStop']
        # ObsStart = metadata['ObsStart']
        # ObsIdCfg = metadata['ObsIdCfg']
        # filename = metadata['filename']
        # AntennaCoordinates = metadata['AntennaCoordinates'][0]
        # AntennaHeight = metadata['AntennaHeight']
        # Fa3 = metadata['Fa3'][0]
        # Fb3 = metadata['Fb3'][0]
        ChanInteTime = 0
        BinNum=2048
        Fa3 = 0
        Fb3 = 0
        Fcenter = 100e6        
        RBW = 200.e6/2048
        ScanDuration = 0
        Span = 200e6
        SweepNum=0
        NumScan = len(timestamp)
        
        # Creation de l'AcqRec
        acq_rec = f.create_group("AcqRec_000")
                
        # Creation du niveau Pointage
        pointing = acq_rec.create_group("Pointing_000")
       
        # Création du niveau Polar 1
        #polar = pointing.create_group("Polar_"+str(self.polar))
        polar0 = pointing.create_group("Polar_0")
        
        # if type_data==0:  #mode Grec
        #     data1= self.Grec1
        #     data2= self.Grec2
            
        #     unit=self.Grec_unit
            
        # if type_data==1:  #mode Gsys
        #     data1= self.Gsys1
        #     data2= self.Gsys2
        #     unit=self.Gsys_unit
                
        # Creation du dataset "Acq"
        acq0 = polar0.create_group("Acq")
        scan=[]
        for i in range(len(timestamp)):
            id_str = str(i)
            nfill = 6
            id_str = id_str.zfill(nfill)
            scan.append(acq0.create_dataset('scan_' +id_str, data=np.pad(data[i,:],((0,BinNum-len(data[i,:]))),mode='empty'))) 
            scan[-1].attrs['FFTchannel0'] = ""
            if type(timestamp[i]) == np.datetime64:
                 scan[-1].attrs['TimeStamp'] = timestamp.astype(object)[i].strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            else:
                 scan[-1].attrs['TimeStamp'] = timestamp[i] #[date.strftime("%Y-%m-%dT%H:%M:%S.%fZ") for date in timestamp]                 
            scan[-1].attrs['nbins'] =str(BinNum)        


        # Creation du dataset Cal
        cal0 = polar0.create_group("Cal")
        scan1=[]
        for i in range(len(timestamp)):
            id_str = str(i)
            nfill = 6 #- len(id_str)+1
            id_str = id_str.zfill(nfill)
            scan1.append(cal0.create_dataset('scan_' +id_str, data=np.pad(data[i,:],((0,BinNum-len(data[i,:]))),mode='empty'))) 
            scan1[-1].attrs['FFTchannel0'] = ""
            if type(timestamp[i]) == np.datetime64:
                 scan1[-1].attrs['TimeStamp'] = timestamp.astype(object)[i].strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            else:
                 scan1[-1].attrs['TimeStamp'] = timestamp[i] #[date.strftime("%Y-%m-%dT%H:%M:%S.%fZ") for date in timestamp]                 
            scan1[-1].attrs['nbins'] =str(BinNum)        
        
        # Création du niveau Polar2
        #polar = pointing.create_group("Polar_"+str(self.polar))
        polar1 = pointing.create_group("Polar_1")

        # Creation du dataset Acq2                
        acq1 = polar1.create_group("Acq")
        scan2=[]
        for i in range(len(timestamp)):
            id_str = str(i)
            nfill = 6 #- len(id_str)+1
            id_str = id_str.zfill(nfill)
            scan2.append(acq1.create_dataset('scan_' +id_str, data=np.pad(data[i,:],((0,BinNum-len(data[i,:]))),mode='empty'))) 
            scan2[-1].attrs['FFTchannel0'] = ""
            if type(timestamp[i]) == np.datetime64:
                 scan2[-1].attrs['TimeStamp'] = timestamp.astype(object)[i].strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            else:
                 scan2[-1].attrs['TimeStamp'] = timestamp[i] #[date.strftime("%Y-%m-%dT%H:%M:%S.%fZ") for date in timestamp]                 
            scan2[-1].attrs['nbins'] =str(BinNum)        


        # Creation du dataset Cal2
        cal1 = polar1.create_group("Cal")
        scan3 = []
        for i in range(len(timestamp)):
            id_str = str(i)
            nfill = 6 #- len(id_str)+1
            id_str = id_str.zfill(nfill)            
            scan3.append(cal1.create_dataset('scan_' +id_str, data=np.ones(BinNum)))
            scan3[-1].attrs['FFTchannel0'] = ""
            if type(timestamp[i]) == np.datetime64:
                 scan3[-1].attrs['TimeStamp'] = timestamp.astype(object)[i].strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            else:
                 scan3[-1].attrs['TimeStamp'] = timestamp[i] #[date.strftime("%Y-%m-%dT%H:%M:%S.%fZ") for date in timestamp]                             
            scan3[-1].attrs['nbins'] =  str(BinNum)
       
        ## Stockage des attributs du fichier HDF5
        ## Niveau 0 (root)
        f.attrs['AntennaCoordinates'] = "" #AntennaCoordinates
        f.attrs['AntennaGain'] = "" #AntennaGain
        f.attrs['AntennaHeight'] = "" #= str(AntennaHeight)
        f.attrs['AntennaType'] = "" #AntennaType

        f.attrs['DataFormat'] = "" #DataFormat
        f.attrs['FileId'] = "CAL" #+FileId    
        f.attrs['ObsIdCfg'] = "" #ObsIdCfg
        if type(timestamp[0]) == np.datetime64:
             f.attrs['ObsStart'] = timestamp.astype(object)[0].strftime("%Y-%m-%dT%H:%M:%S.%fZ")
             f.attrs['ObsStop'] =  timestamp.astype(object)[-1].strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        else:
             f.attrs['ObsStart'] = 0 #ObsStart.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
             f.attrs['ObsStop'] =  2*np.pi #ObsStop.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        f.attrs['ReceiverType'] = "" #ReceiverType
        f.attrs['Scanning'] = "" #Scanning
        f.attrs['StationCode'] = "" #StationCode
        f.attrs['StationName'] = "" #StationName
        f.attrs['Theme'] = "" #Theme
        f.attrs['Title'] = "" #Title
        
        
    
        #self.CfgID        
              

        ## Niveau 1A (AcqRec_xxx)                                       
        acq_rec.attrs['BinNum'] = BinNum
        acq_rec.attrs['Fa3'] = Fa3
        acq_rec.attrs['Fb3'] = Fb3 
        acq_rec.attrs['Fcenter'] = Fcenter
        acq_rec.attrs['Index'] = 0
        acq_rec.attrs['MeasType'] = "Acq"
        acq_rec.attrs['Name'] = "spectre median apres correction ciel" #"simulation GSM ciel"
        acq_rec.attrs['RBW'] = RBW
        acq_rec.attrs['ScanDuration'] = ScanDuration
        acq_rec.attrs['Span'] = Span
        acq_rec.attrs['SweepNum'] = SweepNum
        acq_rec.attrs['VBW'] = ""
        
 
        ## Niveau 2A (Pointing_xxx)               
        pointing.attrs['Azimuth'] = "000000" #Azimuth
        pointing.attrs['Site'] = "" #Site

        ## Niveau 3A (Polar_x)        
        polar0.attrs['Polarization'] = "VL" # !!! TO BE CHECKED !!!!!!!
        
        # if len(self.var_liste_polar2)>1:
        #     polar1.attrs['Polarization'] = self.var_liste_polar2[1]
        # else:
            
        polar1.attrs['Polarization'] ="HL" # !!! TO BE CHECKED !!!!!!


        
        ## Niveau 4A (Cal)            
        cal0.attrs['ChanInteTime'] = ""
        cal0.attrs['DataStatus'] = "raw"
        cal0.attrs['Detector'] = ""
        cal0.attrs['MeasurementAccuracy'] = ""
        cal0.attrs['NumScan'] = NumScan
        cal0.attrs['ScanMode'] = "specific"
        cal0.attrs['Unit'] = unit_str



        cal1.attrs['ChanInteTime'] = ""
        cal1.attrs['DataStatus'] = "raw"
        cal1.attrs['Detector'] = ""
        cal1.attrs['MeasurementAccuracy'] = ""

        
        cal1.attrs['NumScan'] = NumScan
        cal1.attrs['ScanMode'] = "specific"
        cal1.attrs['Unit'] = unit_str        

        cal0.attrs['ChanInteTime'] =ChanInteTime # for compatibility with read_NSA_hdf5
        cal1.attrs['ChanInteTime'] =ChanInteTime # for compatibility with read_NSA_hdf5
        
        ## Niveau 5A (Cal)                       
        #scan.attrs['TimeStamp'] = ""



        acq0.attrs['ChanInteTime'] = ""
        acq0.attrs['DataStatus'] = "raw"
        acq0.attrs['Detector'] = ""
        acq0.attrs['MeasurementAccuracy'] = ""
        acq0.attrs['NumScan'] = NumScan
        acq0.attrs['ScanMode'] = "specific"
        acq0.attrs['Unit'] = unit_str



        acq1.attrs['ChanInteTime'] = ""
        acq1.attrs['DataStatus'] = "raw"
        acq1.attrs['Detector'] = ""
        acq1.attrs['MeasurementAccuracy'] = ""

        
        acq1.attrs['NumScan'] = NumScan
        acq1.attrs['ScanMode'] = "specific"
        acq1.attrs['Unit'] = unit_str        

        acq0.attrs['ChanInteTime'] =ChanInteTime # for compatibility with read_NSA_hdf5
        acq1.attrs['ChanInteTime'] =ChanInteTime # for compatibility with read_NSA_hdf5
        
        ## Niveau 5A (Acq)                       
        #scan.attrs['TimeStamp'] = ""


        



        f.close()

def scan_metadata_ordered(path='/databf2/nsa/obs/observateur/',file_filter='*Mon10-100M_Ro14.hdf5',date_filter=None,outfile=None):
        """
        Scan metadata (attributes) of all hdf5 files in the path/YEAR/MONTH/DATADIR/ subdirs, following the provided file_filter (default:*Mon10-100M_Ro14.hdf5) and date_filter (default: None, set with a tuple of dates, "now" as a date = now)
        outfile : path to an output hdf5 file filled with metadata
        Note 11/05/2020: Tint only
        output: metadata dic (was : TINT, list of files 'matches')
        (Remastered version of scan_metadata 03/2022 to take the new year/month directory structure into account)
        """

        # some dates filter has been given
        date_temp=[]
        if date_filter != None:
          # if "now" string as a date, take the date now
          if type(date_filter[0]) == str:
            date_temp.append(datetime.datetime.now())
          else:
            date_temp.append(to_datetime(date_filter[0]))
          if type(date_filter[1]) == str:
            date_temp.append(datetime.datetime.now())
          else:
            date_temp.append(to_datetime(date_filter[1]))

        print(date_temp)
        i=0
        meta_tot = {}
        list_years = glob.glob(path+"*")
        for YEAR_dir in list_years:
                year = int(YEAR_dir.split('/')[-1])

                list_months = glob.glob(YEAR_dir+'/*')

                if year>=date_temp[0].year and year<=date_temp[1].year:
                    print("> Année : "+str(year))
                    for MONTH_dir in list_months:
                        month = int(MONTH_dir.split('/')[-1])
                        date_current = datetime.datetime(year=year,month=month,day=1,hour=0,minute=0,second=0)

                    
                        if date_current >= date_temp[0].replace(day=1) and date_current <= date_temp[1]:
#                        if month>=date_temp[0].month and month<=date_temp[1].month:
                            print("> Mois : "+str(month))
                            print("> Scan for subdirs : "+MONTH_dir)
                            
                            meta_temp = scan_metadata(path=MONTH_dir+'/',file_filter=file_filter,date_filter=date_filter,outfile=outfile)

                            if meta_temp != None:
                                if i == 0:
                                    meta_tot = meta_temp                            

                                else:
                                    for key in meta_temp.keys():
                                        meta_tot[key].extend(meta_temp[key])
                                i+=1

        print("\n### TOTAL : "+str(len(meta_tot['filename']))+" fichiers.")
        return meta_tot


def scan_metadata(path='/databf2/nsa/obs/observateur/',file_filter='*Mon10-100M_Ro14.hdf5',date_filter=None,outfile=None):
        """
        Scan metadata (attributes) of all hdf5 files in the path and 1st level subdirs following the provided file_filter (default:*Mon10-100M_Ro14.hdf5) and date_filter (default: None, set with a tuple of dates, "now" as a date = now)
        outfile : path to an output hdf5 file filled with metadata
        Note 11/05/2020: Tint only
        output: metadata dic (was : TINT, list of files 'matches')
        """

        #TINT : list of all unique integration time Tint encountered
        Ndash = 40


        # some dates filter has been given
        date_temp=[]
        if date_filter != None:
          # if "now" string as a date, take the date now
          if type(date_filter[0]) == str:
            date_temp.append(datetime.datetime.now())
          else:
            date_temp.append(to_datetime(date_filter[0]))
          if type(date_filter[1]) == str:
            date_temp.append(datetime.datetime.now())
          else:
            date_temp.append(to_datetime(date_filter[1]))

        print(date_temp)

        print("> Subdir scanning for file matching the file_filters...")
        sys.stdout.write("[0 subdirs]\r")
        matches=[]
        Nfile = 0
#        for root, dirnames, filenames in os.walk(path):
#            for filename in fnmatch.filter(filenames, file_filter):
#        if path[-1] != '/':
#          path = path + '/'
        
        list_dir = glob.glob(path+"*")
        for root in list_dir:
                temp = root.split('/')
                try:
                   DD = int(temp[-1][6:8])
                   YY = int(temp[-1][:4])
                   MM = int(temp[-1][4:6])
                   hh = int(temp[-1][9:11])
                   mm = int(temp[-1][11:13])
                   
                   filtered = False
                except:
                   DD = 1
                   YY = 1900
                   MM = 1
                   hh = 0
                   mm = 0
                   filtered = True
                
                if date_filter != None or (YY >= date_temp[0].year and YY <= date_temp[1].year and MM >= date_temp[0].month and MM <= date_temp[1].month) and not filtered: # directory date is compatible with filtering parameters

                   # get the date in the filename for a quick first filtering
                   list_dir2 = glob.glob(root+"/"+file_filter)
                   for root2 in list_dir2:
#                      while temp[1].find('NSA') == -1:
#                           list_dir2 = glob.glob(root2+"/*")
                      temp = root2.split('/')[-1].split('_')

                      if len(list_dir2) >= 1:
                          file_temp = list_dir2[0]
                          matches.append(os.path.join(root2, file_temp))
                      #pudb.set_trace()
                      # YY = int(temp[0][:4])
                      # MM = int(temp[0][4:6])

                      # if temp[0].find('NSA') != -1 and temp[-1].find('hdf5') != -1:
                      #    try:


                      #       hh = int(temp[1][:2])
                      #       mm = int(temp[1][2:])
                      #       print(hh)
                      #       print(mm)
                      #    except:
                      #       filtered = True
                      #       hh=0
                      #       mm=0
                      # else: # problem with the path name, lacking beginning hour
                      #    filtered = True
                      #    hh=0
                      #    mm=0
                      # filedate = datetime.datetime(YY,MM,DD,hh,mm,0) 
                      # if date_filter != None or (filedate >= (date_temp[0]-datetime.timedelta(seconds=24*3600)) and (filedate <= (date_temp[1])+datetime.timedelta(seconds=24*3600))) and not filtered: # take ObsStop +/1 one days for a crude first filter
                      #         filelist = glob.glob(root2+'/'+file_filter)
                      #         if len(filelist) >= 1:
                      #           file_temp = filelist[0]
                      #           matches.append(os.path.join(root2, file_temp))
                      sys.stdout.flush()
                      sys.stdout.write("> "+str(Nfile)+" subdirs scanned ["+str(len(matches))+" files matching]\r")
                      Nfile+=1
        #matches = [matches[0]]
        print('\n') 
       
        Nfile = len(matches)
        if Nfile == 0:
             print("> No file matching the file_filters.")
             return None

        print("> scanning "+str(Nfile)+" hdf5 files...")

        #filename = '/databf/nsa/obs/20190310_0302_NSA20190310-01_0587_Mon10-100M_Ro14/NSA20190310-01_0587_Mon10-100M_Ro14.hdf5'
        #filename = '/databf/nsa/obs/observateur/observations de routine/20200414_0316_NSA20200414-02_0725_Mon10-100M_Ro14/NSA20200414-02_0725_Mon10-100M_Ro14.hdf5'
        #xml_name = '/'.join(filename.split('/')[:-1])+"/dataset.xml"
        #filename.split('/'[:-1]+"dataset.xml")
        AntennaType = []
        ifile = 0
        Tint = []


        if outfile != None:
            fid = h5py.File(outfile,'w')
            
        
        Ndash = 40
        print("> Scanning metadata...")
        sys.stdout.write(">"+"-"*Ndash+" [0%]\r")
        t0 = time.time()

        # to test on a small nb of files
        #temp = []
        #for n in np.round(np.random.rand(10)*(len(matches)-1)):
        #        temp.append(matches[int(n)])
        #matches = temp

        for filename in matches:

            #print "\n> File "+str(ifile+1)+" / "+str(Nfile)
            #print "> "+filename+"\n"
            meta_temp = read_NSA_hdf5(filename,meta_only=True)
            if ifile == 0:
                 meta_tot = {}
                 for key,value in meta_temp.items():
                      meta_tot[key] = [] #np.empty(len(matches),dtype=type(value))

            for key,value in meta_temp.items():
                 if type(value) == datetime.datetime:
                      value = value.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
                 meta_tot[key].append(value)

            Tint.append(meta_temp['Tint'])

            ifile+=1
            frac_done = float(ifile)/(Nfile)
            Ndone = int(np.round(Ndash*frac_done))
            sys.stdout.flush()
            sys.stdout.write("|"*Ndone+">"+"-"*(Ndash-Ndone-1)+" ["+"%d"%(int(frac_done*100))+"%]\r")

        #pudb.set_trace()
        TINT = list(set(np.concatenate(Tint)))
        # save in hdf5, to be finished !!
#        h = h5py.File('test_metadata.hdf5','w')
#        for key,value in meta_tot.items():
#             if type(value) == 
#             h.create_dataset(key,data=value)

        print("\n> "+str(len(TINT))+" different integration times : (in seconds)")
        print(TINT)

        print("> Final filtering based on ObsStart metadata...") 
        meta_tot_temp = {}
        for key,value in meta_tot.items():
          meta_tot_temp[key] = [] #np.empty(len(matches),dtype=type(value))


        i=0
        for dd in meta_tot['ObsStart']:
          dd_date =datetime.datetime.strptime(dd,"%Y-%m-%dT%H:%M:%S.%fZ")
          dd_date2 =datetime.datetime.strptime(meta_tot['ObsStop'][i],"%Y-%m-%dT%H:%M:%S.%fZ")
          if date_filter != None:

#            if dd >= date_temp[0] and dd <= date_temp[1]:
#            if (dd_date <= date_temp[0] and meta_tot['ObsStop'][i] >= date_temp[0]) or (dd >= date_temp[0] and dd <= date_temp[1]):
            if (dd_date <= date_temp[0] and dd_date2 >= date_temp[0]) or (dd_date >= date_temp[0] and dd_date <= date_temp[1]):
              for key in meta_tot.keys():
                meta_tot_temp[key].append(meta_tot[key][i])
                
          else:
              for key in meta_tot.keys():
                meta_tot_temp[key].append(meta_tot[key][i])
                
          i+=1

        print("\n"+str(len(meta_tot_temp['filename']))+" Files selected.")
        if outfile != None:
            for key,value in meta_tot_temp.items():
#                if type(value) == datetime.datetime:
#                      value = value.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

                print(key)
                print(value)
            
                fid[key] = value
            fid.close()

        return meta_tot_temp
                           
################################### end scan_metadata() #############################


def scan_hdf5(metadata,mode='band',Fmin=40.e6,Fmax=45.e6):
        """
        Scan the files listed in filelist for total power between Freq_lo and Freq_hi (Hz) vs. time.
        Inputs:
               Fmin: lower frequency (Hz)
               Fmax: higher frequency (Hz)
               filelist: list of path to the hdf5 files
               metadata : dictionnary with metadata for all files to be scanned
               optional inputs:
               mode: (['band'] | 'median' | 'zoom' | '812' | 'monitoring') either compute power in a given band defined by Fmin and Fmax (Hz), or the  median of each NumScan spectra group, or a concatenation of several zoom bands, or special 812 config with interleaved normal and zoom spectra, or monitoring output with median/min/max/occupancy spectra, baseline model
        Outputs:
               PX, PY: power in the band for polar X and Y, arrays of dimension Nt x Ntint (Ntint = nb of integration times, 2 polars, Nt: number of time samples)
               t: datetimes list
               F: centre frequency
               dF: frequency channel half width
               ifile: index of the file in the provided list
        """

        filelist = metadata['filename']
        Nfile = len(filelist)
        TINT = list(set(np.concatenate(metadata['Tint'])))



        # TOCX_ = []
        # for i in range(len(TINT)):
        #         TOCX_.append([])

        # TOCY_ = []
        # for i in range(len(TINT)):
        #         TOCY_.append([])
        
        PX_ = []
        for i in range(len(TINT)):
                PX_.append([])

        PY_ = []
        for i in range(len(TINT)):
                PY_.append([])
        t_ = []
        for i in range(len(TINT)):
                t_.append([])

        F_ = []
        for i in range(len(TINT)):
                F_.append([])



        if mode == 'monitoring':
            PX_max = []
            for i in range(len(TINT)):
                PX_max.append([])

            PY_max = []
            for i in range(len(TINT)):
                PY_max.append([])

            PX_min = []
            for i in range(len(TINT)):
                PX_min.append([])

            PY_min = []
            for i in range(len(TINT)):
                PY_min.append([])

            baseline_X = []
            for i in range(len(TINT)):
                baseline_X.append([])

            baseline_Y = []
            for i in range(len(TINT)):
                baseline_Y.append([])

            thresh_X = []
            for i in range(len(TINT)):
                thresh_X.append([])

            thresh_Y = []
            for i in range(len(TINT)):
                thresh_Y.append([])


            MAD_X = []
            for i in range(len(TINT)):
                MAD_X.append([])

            MAD_Y = []
            for i in range(len(TINT)):
                MAD_Y.append([])
                
            Toc_X = [] # occupancy
            for i in range(len(TINT)):
                Toc_X.append([])

            Toc_Y = []
            for i in range(len(TINT)):
                Toc_Y.append([])
                
        if mode == "812":
            PX_zoom = []
            for i in range(len(TINT)):
                PX_zoom.append([])

            PY_zoom = []
            for i in range(len(TINT)):
                PY_zoom.append([])
            t_zoom = []
            for i in range(len(TINT)):
                t_zoom.append([])

            t_zoom_s = []
            for i in range(len(TINT)):
                t_zoom_s.append([])

            t_s = []
            for i in range(len(TINT)):
                t_s.append([])                                

            t_sidereal_zoom = []
            for i in range(len(TINT)):
                t_sidereal_zoom.append([])

            t_sidereal0 = []
            for i in range(len(TINT)):
                t_sidereal0.append([])
                
            F_zoom = []
            for i in range(len(TINT)):
                F_zoom.append([])

        ifile_ = []
        for i in range(len(TINT)):
           ifile_.append([])

        Ndash = 40
        print("> Scanning Files...")
        sys.stdout.write(">"+"-"*Ndash+" [0% | 0/"+str(len(filelist))+"]\r")

        t0 = time.time()
        i =0
        for file in filelist:
            
                if mode != 'monitoring':                
                    meta_temp, DATA, TIMESTAMP, AZIMUTH = read_NSA_hdf5(file,stdout=False)
                else: # monitoring mode, analyze the file instead of simply reading it
                    OUT = ana_hdf5(file,19e6,80e6)
                    meta_temp = OUT['metadata'].copy()
                    DATA = OUT['DATA'].copy()
                    TIMESTAMP = OUT['TIMESTAMP'].copy()
                    AZIMUTH = OUT['AZIMUTH'].copy()



                i_acqrec = 0
                for dt in meta_temp['Tint']:
                        
                        k = TINT.index(dt)
                        if mode == "band":
                             imin = np.argmin(np.abs((meta_temp['Freqs'][str(i_acqrec)]-Fmin)))
                             imax = np.argmin(np.abs((meta_temp['Freqs'][str(i_acqrec)]-Fmax)))
                             PX_[k].append(np.sum(DATA[str(i_acqrec)][0,0,:,imin:imax],axis=-1))
                             PY_[k].append(np.sum(DATA[str(i_acqrec)][0,1,:,imin:imax],axis=-1))                        
                             t_[k].append(TIMESTAMP[str(i_acqrec)][0,0,:])
                        elif mode == "median" or mode == "monitoring":
                             PX_[k].append(np.median(DATA[str(i_acqrec)][0,0,:,:],axis=0))
                             PY_[k].append(np.median(DATA[str(i_acqrec)][0,1,:,:],axis=0))
                             t_[k].append(TIMESTAMP[str(i_acqrec)][0,0,0].astype(object)+(TIMESTAMP[str(i_acqrec)][0,0,-1].astype(object)-TIMESTAMP[str(i_acqrec)][0,0,0].astype(object))/2) # middle time of NumScan samples

                        if mode == "monitoring":

                             PX_max[k].append(np.max(DATA[str(i_acqrec)][0,0,:,:],axis=0))
                             PY_max[k].append(np.max(DATA[str(i_acqrec)][0,1,:,:],axis=0))
                             
                             PX_min[k].append(np.min(DATA[str(i_acqrec)][0,0,:,:],axis=0))
                             PY_min[k].append(np.min(DATA[str(i_acqrec)][0,1,:,:],axis=0))
                             baseline_X[k].append(OUT['baseline'][str(i_acqrec)][0,:])
                             baseline_Y[k].append(OUT['baseline'][str(i_acqrec)][1,:])
                             thresh_X[k].append(OUT['thresh_K'][str(i_acqrec)][0,:])
                             thresh_Y[k].append(OUT['thresh_K'][str(i_acqrec)][1,:])
                             MAD_X[k].append(OUT['MAD'][str(i_acqrec)][0,:])
                             MAD_Y[k].append(OUT['MAD'][str(i_acqrec)][1,:])

                             tempX = np.zeros(DATA[str(i_acqrec)].shape[-1])
                             tempY = np.zeros(DATA[str(i_acqrec)].shape[-1])
                             for i in range(DATA[str(i_acqrec)].shape[-1]):
                                 ii_X = find(DATA[str(i_acqrec)][0,0,:,i]>thresh_X[k][-1][i])
                                 tempX[i] = len(ii_X)/DATA[str(i_acqrec)].shape[-2]
                                 ii_Y = find(DATA[str(i_acqrec)][0,1,:,i]>thresh_Y[k][-1][i])
                                 tempY[i] = len(ii_Y)/DATA[str(i_acqrec)].shape[-2]
                             Toc_X[k].append(tempX)
                             Toc_Y[k].append(tempY)

                        elif mode == "zoom":
                            #if dt == 0.9999999776482582:
                               #pudb.set_trace()
                            decim = 100
                            #print("> concatenate frequency...")
                            Freqs = np.concatenate(list(metadata['Freqs'][k].values()))
                            #print("Done.")
                            #print("> concatenate data ...")
                            Mtemp = np.concatenate(list(DATA.values()),axis=-1)
                            #print('Done.')
                            if dt == 0.9999999776482582:
                               t_temp = np.tile(np.concatenate(list(TIMESTAMP.values()),axis=-1).reshape(1,2,500,9),2048)
                               F_temp = np.tile(Freqs,[1,2,500,1])
                            else:
                               t_temp = np.concatenate(list(TIMESTAMP.values()),axis=-1)
                            PX_[k].append(Mtemp[0,0,::decim,:])
                            PY_[k].append(Mtemp[0,1,::decim,:])
                            t_[k].append(t_temp[0,0,::decim,:])
                            F_[k].append(F_temp[0,0,::decim,:])
                        elif mode == "812":
                            F_temp = np.tile(list(meta_temp['Freqs'].values())[::2],[1,2,1,1])
                            F_temp_zoom = np.tile(np.concatenate(list(meta_temp['Freqs'].values())[1::2]),[1,2,10,1])
                            Mtemp_zoom = np.concatenate(list(DATA.values())[1::2],axis=-1)
                            Mtemp = np.concatenate(list(DATA.values())[::2],axis=-1)


                            TIMESTAMP_temp = np.concatenate(list(TIMESTAMP.values())[::2],axis=-1).reshape(1,2,1,24)
                            s = TIMESTAMP_temp.shape
                            TIMESTAMP_temp_s = to_sidereal(TIMESTAMP_temp.flatten().astype(object)).reshape(s)

                            TIMESTAMPz_temp = np.concatenate(list(TIMESTAMP.values())[1::2],axis=-1).reshape(1,2,10,24)
                            s = TIMESTAMPz_temp.shape
                            TIMESTAMPz_temp_s = to_sidereal(TIMESTAMPz_temp.flatten().astype(object)).reshape(s)
                            
#                            t_temp_zoom = np.tile(np.concatenate(list(TIMESTAMP.values())[1::2],axis=-1).reshape(1,2,10,24),2048) # exhcange zoom and routine??
#                            t_temp = np.tile(np.concatenate(list(TIMESTAMP.values())[::2],axis=-1).reshape(1,2,1,24),2048)

                            t_temp_zoom = np.tile(TIMESTAMPz_temp,2048) # exhcange zoom and routine??
                            t_temp = np.tile(TIMESTAMP_temp,2048)

                            t_temp_zoom_s = np.tile(TIMESTAMPz_temp_s,2048) # exhcange zoom and routine??
                            t_temp_s = np.tile(TIMESTAMP_temp_s,2048)
                            
                            PX_[k].append(Mtemp[0,0,:,:])
                            PY_[k].append(Mtemp[0,1,:,:])
                            t_[k].append(t_temp[0,0,:,:])
                            t_s[k].append(t_temp_s[0,0,:,:])                            
                            F_[k].append(F_temp[0,0,:,:])
                            PX_zoom[k].append(Mtemp_zoom[0,0,:,:])
                            PY_zoom[k].append(Mtemp_zoom[0,1,:,:])
                            t_zoom[k].append(t_temp_zoom[0,0,:,:])
                            t_zoom_s[k].append(t_temp_zoom_s[0,0,:,:])
                            F_zoom[k].append(F_temp_zoom[0,0,:,:])

                            
                        ifile_[k].append(np.ones(TIMESTAMP[str(i_acqrec)][0,0,:].shape,dtype=np.int32)*i)
                        i_acqrec+=1
                i+=1
                t1_temp = time.time()
                elapsed = t1_temp-t0
                frac_done = float(i)/(len(filelist))
                remaining = (1-frac_done)*elapsed/frac_done
                Ndone = int(np.round(Ndash*frac_done))
                sys.stdout.flush()
                sys.stdout.write("|"*Ndone+">"+"-"*(Ndash-Ndone-1)+" ["+"%d"%(int(frac_done*100))+"% | "+str(i)+"/"+str(len(filelist))+"] (elapsed: "+"%.1f"%elapsed+" s, ETA: "+"%.1f"%remaining+" s)\r")
                
        F = (Fmax+Fmin)/2
        dF = (Fmax-Fmin)/2

        PX = {}
        PY = {}
        t = {}
        
        # monitoring mode
        PX_MAX = {}
        PY_MAX = {}

        PX_MIN = {}
        PY_MIN = {}
        
        BASELINE_X = {}
        BASELINE_Y = {}

        THRESH_X = {}
        THRESH_Y = {}        

        MAD_XX = {}
        MAD_YY = {}

        TOC_X = {}
        TOC_Y = {}
        ######
        
        PXz = {} # z for zoom (config 812)
        PYz = {}
        tz = {}
        FF={}
        if mode != "812":
             F_zoom = {}
        F_z = {}
        t_sidereal = {}
        t_siderealz = {}
        ifile = {}
        i=0

        for dt in TINT:
                print("> dt = "+str(dt))
                if mode == "band" or mode == "zoom" or mode == "812":
                     #pudb.set_trace()
                     PX[str(dt)] = np.concatenate(PX_[i]).ravel()
                     PY[str(dt)] = np.concatenate(PY_[i]).ravel()
                     t[str(dt)] = np.concatenate(t_[i]).ravel().astype(object)
                     if mode == "zoom":
                         F_zoom[str(dt)] = np.concatenate(F_[i])
                     elif mode == "812":
                         FF[str(dt)] = np.concatenate(F_[i]).ravel()
                         #pudb.set_trace()
                         F_z[str(dt)] = np.concatenate(F_zoom[i]).ravel()
                         PXz[str(dt)] = np.concatenate(PX_zoom[i]).ravel()
                         PYz[str(dt)] = np.concatenate(PY_zoom[i]).ravel()
                         tz[str(dt)] = np.concatenate(t_zoom[i]).ravel().astype(object)
                         t_siderealz[str(dt)] = np.concatenate(t_zoom_s[i]).ravel().astype(object)
                         t_sidereal[str(dt)] = np.concatenate(t_s[i]).ravel().astype(object)
                elif mode == "median" or mode == "monitoring":
                     PX[str(dt)] = np.array(PX_[i])
                     PY[str(dt)] = np.array(PY_[i])
                     t[str(dt)] = np.array(t_[i]).astype(object)

                if mode == "monitoring":
                    PX_MAX[str(dt)] = np.array(PX_max[i])
                    PY_MAX[str(dt)] = np.array(PY_max[i])

                    PX_MIN[str(dt)] = np.array(PX_min[i])
                    PY_MIN[str(dt)] = np.array(PY_min[i])                    

                    BASELINE_X[str(dt)] = np.array(baseline_X[i])
                    BASELINE_Y[str(dt)] = np.array(baseline_Y[i])

                    THRESH_X[str(dt)] = np.array(thresh_X[i])
                    THRESH_Y[str(dt)] = np.array(thresh_Y[i])                    

                    MAD_XX[str(dt)] = np.array(MAD_X[i])
                    MAD_YY[str(dt)] = np.array(MAD_Y[i])
                    pudb.set_trace()
                    TOC_X[str(dt)] = np.array(Toc_X[i])
                    TOC_Y[str(dt)] = np.array(Toc_Y[i])
                    
                if mode != "812":
                    t_sidereal[str(dt)] = to_sidereal(t[str(dt)])


                ifile[str(dt)] = np.concatenate(ifile_[i]).ravel()                                
                i+=1
                if mode == 'median':
                     PX[str(dt)] = PX[str(dt)].reshape(len(t[str(dt)]),len(metadata['Freqs'][0]['0']))
                     PY[str(dt)] = PY[str(dt)].reshape(len(t[str(dt)]),len(metadata['Freqs'][0]['0']))
                     
                if mode == "monitoring":
                    PX_MAX[str(dt)] = PX_MAX[str(dt)].reshape(len(t[str(dt)]),len(metadata['Freqs'][0]['0']))
                    PY_MAX[str(dt)] = PY_MAX[str(dt)].reshape(len(t[str(dt)]),len(metadata['Freqs'][0]['0']))

                    PX_MIN[str(dt)] = PX_MIN[str(dt)].reshape(len(t[str(dt)]),len(metadata['Freqs'][0]['0']))
                    PY_MIN[str(dt)] = PY_MIN[str(dt)].reshape(len(t[str(dt)]),len(metadata['Freqs'][0]['0']))

                    BASELINE_X[str(dt)] = BASELINE_X[str(dt)].reshape(len(t[str(dt)]),len(metadata['Freqs'][0]['0']))
                    BASELINE_Y[str(dt)] = BASELINE_Y[str(dt)].reshape(len(t[str(dt)]),len(metadata['Freqs'][0]['0']))

                    THRESH_X[str(dt)] = THRESH_X[str(dt)].reshape(len(t[str(dt)]),len(metadata['Freqs'][0]['0']))
                    THRESH_Y[str(dt)] = THRESH_Y[str(dt)].reshape(len(t[str(dt)]),len(metadata['Freqs'][0]['0']))

                    MAD_XX[str(dt)] = MAD_XX[str(dt)].reshape(len(t[str(dt)]),len(metadata['Freqs'][0]['0']))
                    MAD_YY[str(dt)] = MAD_YY[str(dt)].reshape(len(t[str(dt)]),len(metadata['Freqs'][0]['0']))                    

                    TOC_X[str(dt)] = TOC_X[str(dt)].reshape(len(t[str(dt)]),len(metadata['Freqs'][0]['0']))
                    TOC_Y[str(dt)] = TOC_Y[str(dt)].reshape(len(t[str(dt)]),len(metadata['Freqs'][0]['0']))
        if mode == "zoom":
            return PX, PY, t,t_sidereal, F_zoom, dF, ifile
        if mode == "812":
            return PX, PXz, PY, PYz, t, tz, t_sidereal, t_siderealz, FF,F_z, dF, ifile
        if mode == 'monitoring':
            polar0 = {}
            polar1 = {}
            polar0['PX'] = PX.copy()
            polar1['PY'] = PY.copy()
            polar0['PX_MAX'] = PX_MAX.copy()
            polar1['PY_MAX'] = PY_MAX.copy()
            polar0['PX_MIN'] = PX_MIN.copy()
            polar1['PY_MIN'] = PY_MIN.copy()                        
            polar0['BASELINE_X'] = BASELINE_X.copy()
            polar1['BASELINE_Y'] = BASELINE_Y.copy()
            polar0['THRESH_X'] = THRESH_X.copy()
            polar1['THRESH_Y'] = THRESH_Y.copy()            
            polar0['MAD_XX'] = MAD_XX.copy()
            polar1['MAD_YY'] = MAD_YY.copy()
            polar0['TOC_X'] = TOC_X.copy()
            polar1['TOC_Y'] = TOC_Y.copy()            

            return polar0, polar1, t,t_sidereal, F, dF, ifile
            
        return PX, PY, t,t_sidereal, F, dF, ifile
    

################################### end scan_hdf5() #############################



def write_monitoring_scan_file(filename,meta_tot,polar0,polar1,t,t_sidereal):
    """
    Write a hdf5 file with data gathered from scanning hdf5 files with scan_hdf5(mode="monitoring")
    """
    f = h5py.File(filename,'w')
    grp_meta = f.create_group("meta_tot")
    print("> Writing Metadata : ")    
    for key in meta_tot.keys():
        print("> "+key)
        if key == 'filename':
             pudb.set_trace()
        if type(meta_tot[key]) == list:
            temp  = f.create_group('meta_tot/'+key)
            for i,value in enumerate(meta_tot[key]):

                if type(value) == dict:
                    temp2  = f.create_group('meta_tot/'+key+"/"+str(i))
                    
                    for key2 in value.keys():
                 
#                        if key2 not in temp.keys():

                        temp2.create_dataset(key2,data=value[key2])
#                        else:
#                            temp[key2]
                else:
                    temp.create_dataset(str(i),data=value)
        
        else:
            f['meta_tot/'+key]=meta_tot[key]


    f.close()
        
def stdMAD(y):
    """
    Compute the  Median Absolute Deviation of 1D signal y, multiplied by a factor for compatibility with standard deviation estimation.
    Output: MAD*k
     with k =1.4826 and MAD = median(abs(y-median(y)))
    """

    k =1.4826
    MAD = np.nanmedian(np.abs(y - np.nanmedian(y)))

    return k*MAD

def stdMAD_recurs(y,display=False):

            N = len(y)
            stdd = stdMAD(y)
            y_med = np.nanmedian(y)
            k = np.sqrt(2*np.log(N))
            y_temp = y.copy().astype(float)

            ii = [0]
            i = 0
            while len(ii) !=0:
                       ii = find(np.abs(y_temp-y_med)>k*stdd)
                       y_temp = np.delete(y_temp,ii)
                       y_med = np.nanmedian(y_temp)
                       stdd = stdMAD(y_temp)
                       i+=1
                       if display:
                           print("> step "+str(i))
            return stdMAD(y_temp)

################################### fin stdMAD() #############################



########################################################################
######################### FITTING  #####################################
########################################################################


def polyfit2d(x, y, z, kx=1, ky=1, order=None): # source: https://stackoverflow.com/questions/33964913/equivalent-of-polyfit-for-a-2d-polynomial-in-python
    '''
    Two dimensional polynomial fitting by least squares.
    Fits the functional form f(x,y) = z.

    Notes
    -----
    Resultant fit can be plotted with:
    np.polynomial.polynomial.polygrid2d(x, y, soln.reshape((kx+1, ky+1)))

    Parameters
    ----------
    x, y: array-like, 1d
        x and y coordinates.
    z: np.ndarray, 2d
        Surface to fit.
    kx, ky: int, default is 3
        Polynomial order in x and y, respectively.
    order: int or None, default is None
        If None, all coefficients up to maxiumum kx, ky, ie. up to and including x^kx*y^ky, are considered.
        If int, coefficients up to a maximum of kx+ky <= order are considered.

    Returns
    -------
    Return paramters from np.linalg.lstsq.

    soln: np.ndarray
        Array of polynomial coefficients.
    residuals: np.ndarray
    rank: int
    s: np.ndarray

    '''

    # grid coords
    X, Y = np.meshgrid(x, y)
    # coefficient array, up to x^kx, y^ky
    coeffs = np.ones((kx+1, ky+1))

    # solve array
    a = np.zeros((coeffs.size, X.size))

    # for each coefficient produce array x^i, y^j
    for index, (i, j) in enumerate(np.ndindex(coeffs.shape)): # NoteB.C.: swapped i and j
        print(index,i,j)
        # do not include powers greater than order
        if order is not None and i + j > order:
            arr = np.zeros_like(X)
        else:
            arr = coeffs[i, j] * X**i * Y**j
        a[index] = arr.ravel()
#        figure()
#        imshow(a[index].reshape(len(y),len(x)),interpolation="None")
#        colorbar()
#        gca().set_aspect('auto')
        
    # do leastsq fitting and return leastsq result
    return np.linalg.lstsq(a.T, np.ravel(z), rcond=None)



def polyfit_recurs(t,P,n=11,plotfig=False,display=False):
            """
            Recursively fit a polynom without outliers.
            """
            N = len(P)
            stdd = stdMAD(P)
            P_med = np.median(P)
            k = np.sqrt(2*np.log(N))
            P_temp = P.copy().astype(float)
            t_temp = t.copy().astype(float)
            
            #ii = find(P_temp>P_med+k*stdd)
            #P_temp = np.delete(P,ii)
            #t_temp = np.delete(t,ii)
            if display:
                print("> recursive polynom fit ...")
            i = 0
            if plotfig:
                        figure()
                        subplot(2,1,1)
                        plot(t,P,'.',markersize=1,label="donnees brutes")
                        subplot(2,1,2)
                        plot(t,P,'.',markersize=2,label="donnees brutes",alpha=0.1)
            ii = [0]
            while len(ii) != 0:
                        #pdb.set_trace()
                        p = np.polyfit(t_temp,P_temp,n)
                        P_fit = np.polyval(p,t_temp) #interpolate.CubicSpline(t_temp,P_temp,bc_type='periodic')[t_temp] => stupid scipy wants python3 for cubicspline..
                        
                        stdd = stdMAD(P_temp - P_fit)
                        ii = find((P_temp-P_fit)>k*stdd)
                        P_temp = np.delete(P_temp,ii)
                        t_temp = np.delete(t_temp,ii)
                        i+=1
                        if plotfig:
                                    subplot(2,1,1)
                                    plot(np.linspace(0,2*np.pi,100),np.polyval(p,np.linspace(0,2*np.pi,100)),label='Iteration '+str(i))
                                    subplot(2,1,2)
                                    plot(np.linspace(0,2*np.pi,100),np.polyval(p,np.linspace(0,2*np.pi,100)),label='Iteration '+str(i),linewidth=2,alpha=0.6)
                                    ylim(np.min(np.polyval(p,np.linspace(0,2*np.pi,100))),np.max(np.polyval(p,np.linspace(0,2*np.pi,100))))
                        if display:
                            sys.stdout.flush()
                            sys.stdout.write("["+str(i)+" steps]\r")
            if display:
                print("\nDone.")
            if plotfig:
                        legend()
                        xlabel('Temps sideral (rad)')
                        ylabel('Power (dB u.a.)')
           
            return p


def polyfit_recurs_remov(t,P,n=5,plotfig=False):
            """
            Recursively fit a polynom excluding outliers and remove the resulting baseline from the input signal
            """
            p = polyfit_recurs(t,P,n=n,plotfig=plotfig)
            Pout = P - np.polyval(p,t)

            return Pout

def scan_polyfit(t,P,N,n=5,MAD=True):
            """
            The input signal P is supposed to be made of groups of N scans. Each group is fitted/corrected for a polynom and the resulting corrected signal is returned.
            If MAD = True, correct only the the groups that are below k*MAD threshold, with k = sqrt(2*log(len(P))) ("universal threshold")
            """
            NN = len(P)//N
            Pout = P.copy()
            MAD = stdMAD(P)
            k = np.sqrt(2*np.log(len(P)))

            for i in range(N):
               correct = True
               if MAD:
                    if len(find(Pout[i*N:(i+1)*N]>k*MAD)) > N/10: # each scan is above threshold => do not correct
                         correct = False
               if (MAD and correct) or not MAD:
#                   Pout[i*N:(i+1)*N] = polyfit_recurs_remov(t[i*N:(i+1)*N],P[i*N:(i+1)*N],n=n)
                    p=polyfit_recurs(t[i*NN:(i+1)*NN],P[i*NN:(i+1)*NN],n=n)
                    Pout[i*NN:(i+1)*NN] = np.polyval(p,t[i*NN:(i+1)*NN])

               else:
                    Pout[i*NN:(i+1)*NN] = P[i*N:(i+1)*N]


            return Pout

def modelfit_recurs(t,P,model,plotfig=False):
            """
            Recursively fit a model without outliers.
            model = NxM array of N components of lentgh M
            """
            N = len(P)
            stdd = stdMAD(P)
            P_med = np.median(P)
            k = np.sqrt(2*np.log(N))
            P_temp = P.copy().astype(float)
            t_temp = t.copy()
            model_temp = model.copy()
            #ii = find(P_temp>P_med+k*stdd)
            #P_temp = np.delete(P,ii)
            #t_temp = np.delete(t,ii)

            ii = np.argsort(t)
            P_temp = P_temp[ii]
            t_temp = t_temp[ii]
            
            #print("> recursively fit a polynom and removing outliers...")
            i = 0
            if plotfig:
                        figure()
                        #subplot(2,1,1)
                        plot(t,P,'.',markersize=1,label="donnees brutes")
                        #subplot(2,1,2)
                        #plot(t,P,'.',markersize=2,label="donnees brutes",alpha=0.1)
            ii = [0]
            while len(ii) != 0:
                        
                        #sol = lsq_linear(model_temp.T,P_temp,bounds=([0,-np.inf],[np.inf,np.inf]))
                        #sol = sol['x']
                        sol = np.dot(np.linalg.pinv(model_temp.T),P_temp)
                        optimal_model = np.dot(sol,model_temp)
                        P_fit = optimal_model.copy() #interpolate.CubicSpline(t_temp,P_temp,bc_type='periodic')[t_temp] => stupid scipy wants python3 for cubicspline..

                        if plotfig:
                                    # subplot(2,1,1)
                                    # plot(t_temp,P_fit,label='Iteration '+str(i))
                                    # subplot(2,1,2)
                                    plot(t_temp,P_fit,label='Iteration '+str(i),linewidth=2,alpha=0.6)
                                    #ylim(32.7,35.5)  
                        stdd = stdMAD(P_temp - P_fit)
                        ii = find((P_temp-P_fit)>k*stdd)
                        P_temp = np.delete(P_temp,ii)
                        t_temp = np.delete(t_temp,ii)
                        model_temp = np.delete(model_temp,ii,axis=1)                        
                        
                        i+=1
                      
                        #sys.stdout.flush()
                        #sys.stdout.write("["+str(i)+" steps]\r")
            #print("\nDone.")
            if plotfig:
                        legend()
                        #xlabel('Temps sideral (rad)')
                        ylabel('Power (u.a.)')
                        title("Best fit: T noise = "+"%.1f"%(sol[0]/sol[1])+r" K, Gain(ua->K) = "+"%.1f"%(1./sol[1]))
                        pudb.set_trace()
           
            return sol





def nonlinfit_recurs(t,P,f,p0,plotfig=False):
            """
            Recursively fit a non linear function f with initial guesses p0 without outliers.
            """
            N = len(P)
            stdd = stdMAD(P)
            P_med = np.median(P)
            k = np.sqrt(2*np.log(N))
            P_temp = P.copy().astype(float)
            t_temp = t.copy().astype(float)
            
            #ii = find(P_temp>P_med+k*stdd)
            #P_temp = np.delete(P,ii)
            #t_temp = np.delete(t,ii)
            
            print("> recursive polynom fit ...")
            i = 0
            if plotfig:
                        figure()
                        subplot(2,1,1)
                        plot(t,P,'.',markersize=1,label="donnees brutes")
                        subplot(2,1,2)
                        plot(t,P,'.',markersize=2,label="donnees brutes",alpha=0.1)
            ii = [0]
            while len(ii) != 0:
                        #pdb.set_trace()
                        p, _ = curve_fit(f,t_temp,P_temp,p0=p0) #[40.,15.e6,0.2e6,8.])
                        P_fit = f(t_temp,*p)
                        
                        stdd = stdMAD(P_temp - P_fit)
                        ii = find((P_temp-P_fit)>k*stdd)
                        P_temp = np.delete(P_temp,ii)
                        t_temp = np.delete(t_temp,ii)
                        i+=1
                        if plotfig:
                                    subplot(2,1,1)
                                    plot(np.linspace(0,2*np.pi,100),np.polyval(p,np.linspace(0,2*np.pi,100)),label='Iteration '+str(i))
                                    subplot(2,1,2)
                                    plot(np.linspace(0,2*np.pi,100),np.polyval(p,np.linspace(0,2*np.pi,100)),label='Iteration '+str(i),linewidth=2,alpha=0.6)
                                    ylim(np.min(np.polyval(p,np.linspace(0,2*np.pi,100))),np.max(np.polyval(p,np.linspace(0,2*np.pi,100))))
                        sys.stdout.flush()
                        sys.stdout.write("["+str(i)+" steps]\r")
            print("\nDone.")
            if plotfig:
                        legend()
                        xlabel('Temps sideral (rad)')
                        ylabel('Power (dB u.a.)')
                       
            return p









def spline_recurs(t,P,k=2,n_knots=10,plotfig=False,display=False):
            """
            Recursively fit splines of order k based on n_knots knots
            """
            N = len(P)
            stdd = stdMAD(P)
            P_med = np.nanmedian(P)
            k = np.sqrt(2*np.log(N)) #*1.5
            P_temp = P.copy().astype(float)
            t_temp = t.copy().astype(float)
            
            #ii = find(P_temp>P_med+k*stdd)
            #P_temp = np.delete(P,ii)
            #t_temp = np.delete(t,ii)
            if display:
                print("> recursive spline fit ...")
            i = 0
            if plotfig:
                        figure()
                        subplot(2,1,1)
                        plot(t,P,'.',markersize=1,label="donnees brutes")
                        subplot(2,1,2)
                        plot(t,P,'.',markersize=2,label="donnees brutes",alpha=0.1)
            ii = [0]
            while len(ii) != 0:
                        #pdb.set_trace()
#                        spl = LSQUnivariateSpline(t_temp,P_temp,t[1::len(t)//10],k=k) # for fitting 14MHz => with zoom spectrum
#                        spl = LSQUnivariateSpline(t_temp,P_temp,t[1::len(t)//30],k=int(k)) # for fitting 20MHz=>80MHz with 100kHz spectrum
#                        pudb.set_trace()
                        spl = LSQUnivariateSpline(t_temp,P_temp,t[1::len(t)//n_knots],k=int(k)) # for fitting 20MHz=>80MHz with 100kHz spectrum
                
                        P_fit = spl(t_temp)
                        
                        stdd = stdMAD(P_temp - P_fit)
                        ii = find(np.abs(P_temp-P_fit)>k*stdd)
                        P_temp = np.delete(P_temp,ii)
                        t_temp = np.delete(t_temp,ii)


                        i+=1
                        if plotfig:
                                    subplot(2,1,1)
#                                    plot(np.linspace(0,2*np.pi,100),np.polyval(p,np.linspace(0,2*np.pi,100)),label='Iteration '+str(i))
                                    plot(np.linspace(0,2*np.pi,100),spl(np.linspace(0,2*np.pi,100)),label='Iteration '+str(i))
                                    subplot(2,1,2)
                                    plot(np.linspace(0,2*np.pi,100),spl(np.linspace(0,2*np.pi,100)),label='Iteration '+str(i),linewidth=2,alpha=0.6)
                                    #ylim(np.min(spl(np.linspace(0,2*np.pi,100))),np.max(spl(np.linspace(0,2*np.pi,100))))
                        if display:
                            sys.stdout.flush()
                            sys.stdout.write("["+str(i)+" steps]\r")
            print("\nDone.")
            if plotfig:
                        legend()
                        xlabel('Temps sideral (rad)')
                        ylabel('Power (dB u.a.)')
                       
            return spl


        
def baseline_ALS(y, lam=6, p=0.05, niter=10):
    """                                                                                                                                                                                                       
    Asymmetric least squares (without too much dependencies, other possibilities with module pybaselines)                                                                                                                                                                                  
    Ref: https://intrepidgeeks.com/tutorial/asymmetric-least-squares-in-python                                                                                                                                
    see pybaselines for other algorithms                                                                                                                                                                      
    Inputs:                                                                                                                                                                                                   
    lam : lambda, 2nd derivative constraint                                                                                                                                                                   
    p : weighting of positive residuals                                                                                                                                                                       
                                                                                                                                                                                                              
    """

    L = len(y)
    D = sparse.diags([1,-2,1],[0,-1,-2], shape=(L,L-2))
    w = np.ones(L)
    for i in range(niter):
        W = sparse.spdiags(w, 0, L, L)
        Z = W + lam * D.dot(D.transpose())
        z = spsolve(Z, w*y)
        w = p * (y > z) + (1-p) * (y < z)
    return z

def baseline_pybaselines(Freq,data,method="drpls"):
    """
    Baeline determination based on some pybaselines module algorithms
       method :
    "drpls" [default] : Doubly Reweighted Penalized Least Squares (pybaselines module, https://pybaselines.readthedocs.io/en/latest/algorithms/whittaker.html?highlight=drpls#drpls-doubly-reweighted-penalized-least-squares)     
    "airpls" : Adaptive Iteratively Reweighted Penalized Least Squares (pybaselines, see link above)                                                                                                                               
    "derpsalsa" : Derivative Peak-Screening Asymmetric Least Squares Algorithm (pybaselines, see link above) 
    """
    baseline_fitter = Baseline(x_data=Freq,check_finite=False)
    

    if method == "drpls":

        baseline = baseline_fitter.drpls(data) # best based on stdMAD of residuals
        
    elif method == "airpls":
        baseline = baseline_fitter.pspline_airpls(data)
        
    elif method == "derpsalsa":
        baseline = baseline_fitter.pspline_derpsalsa(data)





    return baseline[0]

        

def fit_spectrum(F,data,n=11,n_knots=10,mode='polyfit'):

    ii_notinf = find(~np.isinf(data))
    data = data[ii_notinf]
    F = F[ii_notinf]
    #data = medfilt(data,kernel_size=501)
    ii_baseline_OK = range(len(data))
    for i in range(1):
        # First rough polynomial fit using my "_recurs" functions, for initializing optimization
        #p = polyfit_recurs(F[ii_baseline_OK],data[ii_baseline_OK],n=n,plotfig=True)
        # remove the polynomial baseline
        data_lines = data.copy()# - scan_polyfit(F[ii_baseline_OK],data[ii_baseline_OK],5,n=1) #np.polyval(p,F)

        
#        pudb.set_trace()
        # define a filter
        b=np.concatenate((np.ones(5),-np.ones(5)))/5.
        a = 1

        # create a trigger signal with simple integrate-differentiate filter
#        z0 = lfiltic(b,a,np.zeros(len(b)))
#        zi = lfilter_zi(b,a)
        data_lines_temp = np.concatenate((data_lines[len(b)-1::-1],data_lines)) # I don't manage to set efficient initial conditions with lfiltic, hence the hack B.C. 01/2021
        data_lines_trig = lfilter(b,a,data_lines_temp) 
        data_lines_trig = data_lines_trig[len(b):]
#        data_lines_trig = filtfilt(b,a,data_lines # filtfilt better for initial conditions, but different pulse shape, implies changing the indices sorting process)
        N = len(data_lines_trig)
        k = np.sqrt(2*np.log(N)) #4 # for zoom, 14MHz => Fhi
        print("k : "+str(k))
#        k = 8 # for low res, 50MHz => Fhi
        stdd = stdMAD_recurs(data_lines_trig)
        #figure()
        # plot(F,data)
        # plot(F,data_lines)
        #plot(F,data_lines_trig)
        #plot(F,np.ones(len(F))*k*stdd,'k--')
        #plot(F,np.ones(len(F))*-k*stdd,'k--')
        # print(find(np.isnan(data_lines)))
        #input()

        ii_pos=find(data_lines_trig>k*stdd)
        ii_neg=find(data_lines_trig<-k*stdd)
        ii_baseline_OK = find(np.abs(data_lines_trig)<=k*stdd)
        ii_diff_pos = np.diff(ii_pos)
        ii_diff_neg = np.diff(ii_neg)
        iii_pos = find(abs(ii_diff_pos)!=1)
        iii_neg = find(abs(ii_diff_neg)!=1)
#        p = polyfit_recurs(F[ii_baseline_OK],data[ii_baseline_OK],n=n)
#        baseline = np.polyval(p,F)

#        spl = LSQUnivariateSpline(F[np.sort(ii_baseline_OK)],data[np.sort(ii_baseline_OK)],F[np.sort(ii_baseline_OK)][1::400],k=2)

        
        # def sigmoid (x, A, h, slope, C,a,b,c,d,ymin):
        #     y = a*x**4+b*x**3+c*x**2+d*x+((1 / (1 + np.exp ((x - h) / slope))) *  A + C)
        #     #y[y<=ymin] = ymin
        #     return y

        # psim, _ = curve_fit(sigmoid,F[ii_baseline_OK],data[ii_baseline_OK],p0=[-2.88105367e+01,1.49310369e+07,1.14946656e+06 ,3.54106588e+01,1.,1.,1.,1.,10.]) #[40.,15.e6,0.2e6,8.])
        # baseline = sigmoid(F,*psim)
        # #print(psim)


        #p = polyfit_recurs(F[ii_baseline_OK],data[ii_baseline_OK]-baseline[ii_baseline_OK],n=4)

        #baseline = baseline + np.polyval(p,F)
        
        #p = nonlinfit_recurs(F[ii_baseline_OK],data[ii_baseline_OK],sigmoid,[-2.88105367e+01,1.49310369e+07,1.14946656e+06 ,3.54106588e+01,1.,1.,1.,1.,10.])
        #baseline = sigmoid(F,*p)

        
        
#       from scipy.interpolate import Rbf
#        f = Rbf(F[ii_baseline_OK],data[ii_baseline_OK],smooth=1)

        # figure()
        # plot(F,data)
        # plot(F,data_lines)
        # plot(F,data_lines_trig)
        # plot(F,np.ones(len(F))*k*stdd,'k--')
        # print(k*stdd)
        # print(find(np.isnan(data)))
        # input()        

        # find beginning and end of each trigged period
        ii_fin_pos = np.zeros(len(ii_pos[iii_pos])+1,dtype=int)
        ii_fin_pos[:-1] = ii_pos[iii_pos]
        ii_fin_pos[-1] = ii_pos[-1]

        ii_deb_pos = np.zeros(len(ii_pos[iii_pos+1])+1,dtype=int)
        ii_deb_pos[1:] = ii_pos[iii_pos+1]
        ii_deb_pos[0] = ii_pos[0]

        ii_fin_neg = np.zeros(len(ii_neg[iii_neg])+1,dtype=int)
        ii_fin_neg[:-1] = ii_neg[iii_neg]
        ii_fin_neg[-1] = ii_neg[-1]

        ii_deb_neg = np.zeros(len(ii_neg[iii_neg+1])+1,dtype=int)
        ii_deb_neg[1:] = ii_neg[iii_neg+1]
        ii_deb_neg[0] = ii_neg[0]

    # zero crossing is between ii_fin_pos and ii_deb_neg
    # line width rather between ii_deb_pos and ii_fin_neg

        ii_cross_deb = ii_fin_pos
        ii_cross_fin = ii_deb_neg

        ii_line_deb = ii_deb_pos
        ii_line_fin = ii_fin_neg

        # clean : no deb without a fin or fin without a deb

        ii_line = []
        ii_cross = []

        mat_line_debfin = np.zeros((len(ii_line_deb)+len(ii_line_fin),2),dtype=int)
        mat_line_debfin[:len(ii_line_deb),0] = ii_line_deb
        mat_line_debfin[:len(ii_line_deb),1] = 0
        mat_line_debfin[len(ii_line_deb):,0] = ii_line_fin
        mat_line_debfin[len(ii_line_deb):,1] = 1

        mat_cross_debfin = np.zeros((len(ii_cross_deb)+len(ii_cross_fin),2),dtype=int)
        mat_cross_debfin[:len(ii_cross_deb),0] = ii_cross_deb
        mat_cross_debfin[:len(ii_cross_deb),1] = 0
        mat_cross_debfin[len(ii_cross_deb):,0] = ii_cross_fin
        mat_cross_debfin[len(ii_cross_deb):,1] = 1

        ii_sort_line = np.argsort(mat_line_debfin[:,0])
        ii_sort_cross = np.argsort(mat_cross_debfin[:,0])

        mat_line_debfin = mat_line_debfin[ii_sort_line,:]
        mat_cross_debfin = mat_cross_debfin[ii_sort_cross,:]

        # remove beginnings without ends end vice versa : lines
        last = 1 # 1 for end, 0 for begin
        i = 0
        while i < mat_line_debfin.shape[0]:
            current_type = mat_line_debfin[i,1]
            current = mat_line_debfin[i,0]
            if i != mat_line_debfin.shape[0]-1:
                next_type = mat_line_debfin[i+1,1]
                next = mat_line_debfin[i+1,0]

            else:
                next_type = np.nan
                next = len(ii_line_deb)-1

            if current_type == 0 and current_type != next_type: # OK, it's not two ends or two beginnings one after the other
                ii_line.append([current,next])
                i+=2
            else:
                i+=1


        # remove beginnings without ends end vice versa : zero-crossings
        last = 1 # 1 for end, 0 for begin
        i = 0
        while i < mat_cross_debfin.shape[0]:
            current_type = mat_cross_debfin[i,1]
            current = mat_cross_debfin[i,0]
            if i != mat_cross_debfin.shape[0]-1:
                next_type = mat_cross_debfin[i+1,1]
                next = mat_cross_debfin[i+1,0]

            else:
                next_type = np.nan
                next = len(ii_cross_deb)-1

            if current_type == 0 and current_type != next_type: # OK, it's not two ends or two beginnings one after the other
                ii_cross.append([current,next])
                i+=2
            else:
                i+=1

        ii_line = np.array(ii_line)
        ii_cross = np.array(ii_cross)

        ii_line_deb = ii_line[:,0]
        ii_line_fin = ii_line[:,1]

        ii_cross_deb = ii_cross[:,0]
        ii_cross_fin = ii_cross[:,1]

        ii_baseline_OK = np.arange(len(F),dtype=np.float)
        for i,ii in enumerate(ii_line_deb):
            ii_baseline_OK[ii_line_deb[i]:ii_line_fin[i]] = np.nan
        
        ii_baseline_OK = ii_baseline_OK[~np.isnan(ii_baseline_OK)]
        ii_baseline_OK = ii_baseline_OK.reshape(len(ii_baseline_OK)).astype(int)
        #ii_baseline_OK = find(np.abs(data_lines_trig)<=k*stdd)        

        spl = spline_recurs(F[np.sort(ii_baseline_OK)],data[np.sort(ii_baseline_OK)],n_knots=n_knots)
        baseline = spl(F)


        
        figure()
        plot(F,data)

        plot(F[ii_cross_deb-5],data[ii_cross_deb-5],'g>',label="début max")
        plot(F[ii_cross_fin-5],data[ii_cross_fin-5],'g<',label='fin max')
        plot(F[ii_line_deb-5],data[ii_line_deb-5],'b>',label="début raie")
        plot(F[ii_line_fin-5],data[ii_line_fin-5],'b<',label='fin raie')

        plot(F[ii_baseline_OK-5],data[ii_baseline_OK-5],'r.',label='sélectionné pour fit baseline')
#        plot(F,np.polyval(p,F),'r--',label="estimated baseline")
#        plot(F,f(F),'r--',label="estimated baseline")
#        plot(F,sigmoid(F,*psim),'r--',label="estimated baseline")
        plot(F,baseline,'k--',label="estimated baseline")
        legend()
        xlabel('Frequence (MHz)')
        ylabel('Puissance (dBua)')
        return baseline
    
        # figure()
        # plot(F,np.ones(len(F))*k*stdd,'k--')
        # plot(F,data_lines_trig)

        # plot(F[ii_cross_deb],data_lines_trig[ii_cross_deb],'r>')
        # plot(F[ii_cross_fin],data_lines_trig[ii_cross_fin],'r<')
        # plot(F[ii_line_deb],data_lines_trig[ii_line_deb],'b>')
        # plot(F[ii_line_fin],data_lines_trig[ii_line_fin],'b<')
        












def fit_spectrum2(F,data,n=11,n_knots=10,mode='polyfit',plotfig=True,max_output = False):

    ii_notinf = find(np.logical_and(~np.isinf(data),~np.isnan(data)))
    data = data[ii_notinf]
    F = F[ii_notinf]
    baseline=np.zeros(len(data))
    #data = medfilt(data,kernel_size=501)
    ii_baseline_OK = range(len(data))


    for i in range(3):
        
        # First rough polynomial fit using my "_recurs" functions, for initializing optimization
        #p = polyfit_recurs(F[ii_baseline_OK],data[ii_baseline_OK],n=n,plotfig=True)
        # remove the polynomial baseline
        data_lines = data.copy()-baseline# - scan_polyfit(F[ii_baseline_OK],data[ii_baseline_OK],5,n=1) #np.polyval(p,F)

        
#        pudb.set_trace()
        # define a filter
        b=np.concatenate((np.ones(5),-np.ones(5)))/5.
        a = 1

        # create a trigger signal with simple integrate-differentiate filter
#        z0 = lfiltic(b,a,np.zeros(len(b)))
#        zi = lfilter_zi(b,a)
        data_lines_temp = np.concatenate((data_lines[len(b)-1::-1],data_lines)) # I don't manage to set efficient initial conditions with lfiltic, hence the hack B.C. 01/2021
        data_lines_trig = lfilter(b,a,data_lines_temp) 
        data_lines_trig = data_lines_trig[len(b):]
#        data_lines_trig = filtfilt(b,a,data_lines # filtfilt better for initial conditions, but different pulse shape, implies changing the indices sorting process)
        N = len(data_lines_trig)
        k = np.sqrt(2*np.log(N)) #4 # for zoom, 14MHz => Fhi
#        k = 8 # for low res, 50MHz => Fhi
        stdd = stdMAD_recurs(data_lines_trig)
        # figure()
        # plot(F,data)
        # plot(F,data_lines)
        # plot(F,data_lines_trig)
        # plot(F,np.ones(len(F))*k*stdd,'k--')
        # plot(F,np.ones(len(F))*-k*stdd,'k--')
        # print(find(np.isnan(data_lines)))
        # input()
        ii_pos=find(data_lines_trig>k*stdd)
        ii_neg=find(data_lines_trig<-k*stdd)
        ii_baseline_OK = find(np.abs(data_lines_trig)<=k*stdd)
        ii_diff_pos = np.diff(ii_pos)
        ii_diff_neg = np.diff(ii_neg)
        iii_pos = find(abs(ii_diff_pos)!=1)
        iii_neg = find(abs(ii_diff_neg)!=1)
#        p = polyfit_recurs(F[ii_baseline_OK],data[ii_baseline_OK],n=n)
#        baseline = np.polyval(p,F)

#        spl = LSQUnivariateSpline(F[np.sort(ii_baseline_OK)],data[np.sort(ii_baseline_OK)],F[np.sort(ii_baseline_OK)][1::400],k=2)

        
        # def sigmoid (x, A, h, slope, C,a,b,c,d,ymin):
        #     y = a*x**4+b*x**3+c*x**2+d*x+((1 / (1 + np.exp ((x - h) / slope))) *  A + C)
        #     #y[y<=ymin] = ymin
        #     return y

        # psim, _ = curve_fit(sigmoid,F[ii_baseline_OK],data[ii_baseline_OK],p0=[-2.88105367e+01,1.49310369e+07,1.14946656e+06 ,3.54106588e+01,1.,1.,1.,1.,10.]) #[40.,15.e6,0.2e6,8.])
        # baseline = sigmoid(F,*psim)
        # #print(psim)


        #p = polyfit_recurs(F[ii_baseline_OK],data[ii_baseline_OK]-baseline[ii_baseline_OK],n=4)

        #baseline = baseline + np.polyval(p,F)
        
        #p = nonlinfit_recurs(F[ii_baseline_OK],data[ii_baseline_OK],sigmoid,[-2.88105367e+01,1.49310369e+07,1.14946656e+06 ,3.54106588e+01,1.,1.,1.,1.,10.])
        #baseline = sigmoid(F,*p)

        
        
#       from scipy.interpolate import Rbf
#        f = Rbf(F[ii_baseline_OK],data[ii_baseline_OK],smooth=1)

        # figure()
        # plot(F,data)
        # plot(F,data_lines)
        # plot(F,data_lines_trig)
        # plot(F,np.ones(len(F))*k*stdd,'k--')
        # print(k*stdd)
        # print(find(np.isnan(data)))
        # input()        

        # find beginning and end of each trigged period
        ii_fin_pos = np.zeros(len(ii_pos[iii_pos])+1,dtype=int)
        ii_fin_pos[:-1] = ii_pos[iii_pos]
        ii_fin_pos[-1] = ii_pos[-1]

        ii_deb_pos = np.zeros(len(ii_pos[iii_pos+1])+1,dtype=int)
        ii_deb_pos[1:] = ii_pos[iii_pos+1]
        ii_deb_pos[0] = ii_pos[0]

        ii_fin_neg = np.zeros(len(ii_neg[iii_neg])+1,dtype=int)
        ii_fin_neg[:-1] = ii_neg[iii_neg]
        ii_fin_neg[-1] = ii_neg[-1]

        ii_deb_neg = np.zeros(len(ii_neg[iii_neg+1])+1,dtype=int)
        ii_deb_neg[1:] = ii_neg[iii_neg+1]
        ii_deb_neg[0] = ii_neg[0]

    # zero crossing is between ii_fin_pos and ii_deb_neg
    # line width rather between ii_deb_pos and ii_fin_neg

        ii_cross_deb = ii_fin_pos
        ii_cross_fin = ii_deb_neg

        ii_line_deb = ii_deb_pos
        ii_line_fin = ii_fin_neg

        # clean : no deb without a fin or fin without a deb

        ii_line = []
        ii_cross = []

        mat_line_debfin = np.zeros((len(ii_line_deb)+len(ii_line_fin),2),dtype=int)
        mat_line_debfin[:len(ii_line_deb),0] = ii_line_deb
        mat_line_debfin[:len(ii_line_deb),1] = 0
        mat_line_debfin[len(ii_line_deb):,0] = ii_line_fin
        mat_line_debfin[len(ii_line_deb):,1] = 1

        mat_cross_debfin = np.zeros((len(ii_cross_deb)+len(ii_cross_fin),2),dtype=int)
        mat_cross_debfin[:len(ii_cross_deb),0] = ii_cross_deb
        mat_cross_debfin[:len(ii_cross_deb),1] = 0
        mat_cross_debfin[len(ii_cross_deb):,0] = ii_cross_fin
        mat_cross_debfin[len(ii_cross_deb):,1] = 1

        ii_sort_line = np.argsort(mat_line_debfin[:,0])
        ii_sort_cross = np.argsort(mat_cross_debfin[:,0])

        mat_line_debfin = mat_line_debfin[ii_sort_line,:]
        mat_cross_debfin = mat_cross_debfin[ii_sort_cross,:]

        # remove beginnings without ends end vice versa : lines
        last = 1 # 1 for end, 0 for begin
        i = 0
        while i < mat_line_debfin.shape[0]:
            current_type = mat_line_debfin[i,1]
            current = mat_line_debfin[i,0]
            if i != mat_line_debfin.shape[0]-1:
                next_type = mat_line_debfin[i+1,1]
                next = mat_line_debfin[i+1,0]

            else:
                next_type = np.nan
                next = len(ii_line_deb)-1

            if current_type == 0 and current_type != next_type: # OK, it's not two ends or two beginnings one after the other
                ii_line.append([current,next])
                i+=2
            else:
                i+=1


        # remove beginnings without ends end vice versa : zero-crossings
        last = 1 # 1 for end, 0 for begin
        i = 0
        while i < mat_cross_debfin.shape[0]:
            current_type = mat_cross_debfin[i,1]
            current = mat_cross_debfin[i,0]
            if i != mat_cross_debfin.shape[0]-1:
                next_type = mat_cross_debfin[i+1,1]
                next = mat_cross_debfin[i+1,0]

            else:
                next_type = np.nan
                next = len(ii_cross_deb)-1

            if current_type == 0 and current_type != next_type: # OK, it's not two ends or two beginnings one after the other
                ii_cross.append([current,next])
                i+=2
            else:
                i+=1

        ii_line = np.array(ii_line)
        ii_cross = np.array(ii_cross)

        ii_line_deb = ii_line[:,0]
        ii_line_fin = ii_line[:,1]

        ii_cross_deb = ii_cross[:,0]
        ii_cross_fin = ii_cross[:,1]

        ii_baseline_OK = np.arange(len(F),dtype=np.float)
        for i,ii in enumerate(ii_line_deb):
            ii_baseline_OK[ii_line_deb[i]:ii_line_fin[i]] = np.nan

        ii_baseline_OK = ii_baseline_OK[~np.isnan(ii_baseline_OK)]
        ii_baseline_OK = ii_baseline_OK.reshape(len(ii_baseline_OK)).astype(int)
        #ii_baseline_OK = find(np.abs(data_lines_trig)<=k*stdd)        

        spl = spline_recurs(F[np.sort(ii_baseline_OK)],data[np.sort(ii_baseline_OK)],n_knots=n_knots)
        baseline = spl(F)

        res = data-baseline
        stdd_res = stdMAD_recurs(res) # final thresholdin on residuals
        ii_baseline_OK = find(res<np.sqrt(2*np.log(N))*stdd_res)

        spl = spline_recurs(F[np.sort(ii_baseline_OK)],data[np.sort(ii_baseline_OK)],n_knots=n_knots)
        baseline = spl(F)

        iimax = []
        max_list = []
        for kk in range(len(ii_line_deb)):
           if ii_line_deb[kk]-5 > 0 and ii_line_deb[kk] < ii_line_fin[kk]:
              iimax.append(np.argmax(data[ii_line_deb[kk]-5:ii_line_fin[kk]-5])+ii_line_deb[kk]-5) # find index of max around the zero cross
              max_list.append(np.max(10**((data[ii_line_deb[kk]-5:ii_line_fin[kk]-5])/10))-10**(baseline[iimax[-1]]/10)) # find value of max minus the continuous background
        

        iimax = np.array(iimax)

        # figure()

        # plot(F,res,'b')
        # plot(F[ii_baseline_OK],res[ii_baseline_OK],'g.')
        
        # plot([F[0],F[-1]],[np.sqrt(2*np.log(N))*stdd_res,np.sqrt(2*np.log(N))*stdd_res],'k--')
        # plot([F[0],F[-1]],[-np.sqrt(2*np.log(N))*stdd_res,-np.sqrt(2*np.log(N))*stdd_res],'k--')
        # input()
    if plotfig:
        figure()
        plot(F,data)

        plot(F[ii_cross_deb-5],data[ii_cross_deb-5],'r>',label="début max")
        plot(F[ii_cross_fin-5],data[ii_cross_fin-5],'r<',label='fin max')
        plot(F[ii_line_deb-5],data[ii_line_deb-5],'b>',label="début raie")
        plot(F[ii_line_fin-5],data[ii_line_fin-5],'b<',label='fin raie')

    #        plot(F[ii_baseline_OK-5],data[ii_baseline_OK-5],'r.',label='sélectionné pour fit baseline')
        plot(F[ii_baseline_OK],data[ii_baseline_OK],'g.',label='sélectionné pour fit baseline')
    #        plot(F,np.polyval(p,F),'r--',label="estimated baseline")
    #        plot(F,f(F),'r--',label="estimated baseline")
    #        plot(F,sigmoid(F,*psim),'r--',label="estimated baseline")
        plot(F,baseline,'k--',label="estimated baseline")
        legend()
        xlabel('Frequence (MHz)')
        ylabel('Puissance (dBua)')
    if max_output:
       return baseline, spl, iimax ,max_list

    return baseline,spl
    
        # figure()
        # plot(F,np.ones(len(F))*k*stdd,'k--')
        # plot(F,data_lines_trig)

        # plot(F[ii_cross_deb],data_lines_trig[ii_cross_deb],'r>')
        # plot(F[ii_cross_fin],data_lines_trig[ii_cross_fin],'r<')
        # plot(F[ii_line_deb],data_lines_trig[ii_line_deb],'b>')
        # plot(F[ii_line_fin],data_lines_trig[ii_line_fin],'b<')



def interp_freq(F0,spectrum,F):
    """
    returns the interpolated spectrum over frequencies F (Hz) (spectrum initially defined over frequencies F0 (Hz), and F is included in F0)
    """
    ii_notinf = find(np.logical_and(~np.isinf(spectrum),~np.isnan(spectrum)))
    data = spectrum[ii_notinf]
    F0 = F0[ii_notinf]
    pudb.set_trace()
    F0_max = np.max(F0)
    F0_min = np.min(F0)

    F_max = np.max(F)
    F_min = np.min(F)

    if F_max > F0_max or F_min < F0_min:
        print('\n!! requested frequency axis is out of the input band and cannot be interpolated, exiting. ')
        return None
    
    spl = InterpolatedUnivariateSpline(F0,data) #,bbox=[F_min,F_max])    

    return spl(F)
    
def gaussienne(x,mu,sigma,ampl):

    return ampl/(sigma*np.sqrt(2*np.pi))*np.exp(-(x-mu)**2/(2*sigma**2))


def gaussiennes(x,*args):
    mus = args[::3]
    sigmas = args[1::3]
    ampls = args[2::3]

    y = np.zeros(len(x))
    for i in range(len(mus)):
        y += ampls[i]/(sigmas[i]*np.sqrt(2*np.pi))*np.exp(-(x-mus[i])**2/(2*sigmas[i]**2))

    return y


# def fit_gauss(x,y)
#         psim, _ = curve_fit(sigmoid,F[ii_baseline_OK],data[ii_baseline_OK],p0=[-2.88105367e+01,1.49310369e+07,1.14946656e+06 ,3.54106588e+01,1.,1.,1.,1.,10.]) #[40.,15.e6,0.2e6,8.])
#         baseline = sigmoid(F,*psim)

        
######################################################################################################
########################################## end FITTING ###############################################
######################################################################################################



def to_sidereal(t,lon=2.19639, lat=47.3797):
            """
            Convert the list of datetime t to sidereal time (in radians) given the longitude and latitude in degrees.
            """
            if type(t) == list:
                        t = np.array(t)
            print("> Converting to sidereal time ...")
            t_sidereal = t.copy()
            obs = ephem.Observer()
            obs.lon = lon/180*np.pi #'2:11:35'
            obs.lat = lat/180*np.pi #'47:22:35'
            i=0
            Ndash = 40
            sys.stdout.write(">"+"-"*Ndash+" [0%]\r")

            for date in t:
                        obs.date = date
                        t_sidereal[i] = obs.sidereal_time()
                        i+=1
                        frac_done = float(i)/(len(t))
                        Ndone = int(np.round(Ndash*frac_done))
                        sys.stdout.flush()
                        sys.stdout.write("|"*Ndone+">"+"-"*(Ndash-Ndone-1)+" ["+"%d"%(int(frac_done*100))+"%]\r")

            return t_sidereal.astype(float)


            # N = 1000 # nuber of points for one sidereal day
            # P_med = np.zeros(N)

            # t_sidereal_grid = np.linspace(0,2*np.pi,N)
            # dts = t_sidereal_grid[1] - t_sidereal_grid[0]
            # for i in range(N):
            #             ii = find(np.logical_and.reduce((t_sidereal>=i*dts,t_sidereal<(i+1)*dts)))
            #             P_med[i] = np.median(P[ii])
                        
            # return P_med,t_sidereal,t_sidereal_grid
                        
            # P = P[np.argsort(t)]
            # t = t[np.argsort(t)]
            # t_sidereal = t_sidereal[np.argsort(t)]
            
            # date_temp_begin = t[0]
            # date_temp_end = t[0]
            # P_grid = []
            # dates_grid = []

            # t_begin = t[0]
            # t_end = t[-1]
            # totsec = (t_end-t_begin).total_seconds()
            # totstep = float(totsec)/dt

            # Ndash = 40
            # print("> Reading data...")
            # sys.stdout.write(">"+"-"*Ndash+" [0%]\r")
            # i=0
            # while date_temp_end < t[-1]:
            #         date_temp_end = date_temp_begin + datetime.timedelta(seconds=dt)
            #         ii = find(np.logical_and.reduce((t>=date_temp_begin,t<=date_temp_end)))

            #         date_temp2 = date_temp_begin
            #         Ptemp = np.nan
            #         if len(ii) !=0:
            #                 Ptemp = np.mean(P[ii])
            #         P_grid.append(Ptemp)
            #         dates_grid.append(date_temp_begin+datetime.timedelta(seconds=dt/2))
            #         date_temp_begin = date_temp_end
            #         i+=1
            #         frac_done = float(i)/(totstep)
            #         Ndone = int(np.round(Ndash*frac_done))
            #         sys.stdout.flush()
            #         sys.stdout.write("|"*Ndone+">"+"-"*(Ndash-Ndone-1)+" ["+"%d"%(int(frac_done*100))+"%]\r")
                                         
            # return P_grid, dates_grid

def to_sameday(t,date0=datetime.datetime(2000,1,1,0,0,0)):
            """
            Translate list of datetimes t into a list of datetimes with same hour,min,sec but on the same date0 day.
            """

            dates = t
            date0 = to_datetime(date0)
            dates = [d.replace(day=date0.day,month=date0.month,year=date0.year) for d in dates]
            return dates

def to_datetime(date):
            """
            Test wether input date is a datetime, if not convert it to datetime
            """
            if type(date) != datetime.datetime:
        	        date = datetime.datetime(date[0],date[1],date[2],date[3],date[4],date[5])
            return date
def to_timestamp(datelist):
            out = []
            for date in datelist:
                 out.append(date.timestamp())
            return np.array(out)


def correct_periodic_sidereal(t,t_sidereal,P,min_gal=4907.777622515594, max_gal= 8397.275251535075,n=0,no_offset=False):
#def correct_periodic_sidereal(t,t_sidereal,P,min_gal=11204.911128488942,max_gal=19182.371149664377,n=0):
#def correct_periodic_sidereal(t,t_sidereal,P,max_gal=13125.7716611,min_gal=6791.64819438):

            #P = P - np.median(P) + stdMAD(P)*np.sqrt(5e6*1.)

            ii_sort = np.argsort(t)
            t = t[ii_sort]
            t_sidereal = t_sidereal[ii_sort]
            P = P[ii_sort]
            
            p = polyfit_recurs(t_sidereal,P)
            P_periodic = np.polyval(p,t_sidereal)
            Pcorr = P - P_periodic + np.mean(P_periodic)
            Pcorr2 = Pcorr.copy()
            Pcorr3 = Pcorr.copy()            

            model_temp = np.polyval(p,np.linspace(0,2*np.pi,1000))
            model_temp = (model_temp - np.min(model_temp))/(np.max(model_temp)-np.min(model_temp)) # normalized to amplitude 1 between 0 and 1
            model_temp = (max_gal-min_gal)*model_temp+min_gal # normalized to provided galactic min-max temperatures between min_gal and max_gal

            model_mean = np.mean(model_temp)
            print('> Average Sky temp (K):')
            print(model_mean)

            tdiff = np.abs(np.diff(t_sidereal))
#            tdiff = np.abs(np.diff(np.ceil((np.cos(t_sidereal/2)))))

            #trig = t_sidereal//(np.pi/2)
            #tdiff = np.abs(np.diff(trig))

            ii = find(tdiff>np.pi)
            Noverlap = 0 # nb of overlaps added between 2 sideral days, total number of fit per day is Noverlap+1
            iii_old = 0
            interval = []
            date_center = []
            for iii in ii:

                        if abs(t_sidereal[iii]-t_sidereal[iii_old]) > np.pi and (t[iii] - t[iii_old]).days<=1./(Noverlap+1):
                                    interval.append([iii_old,iii])
                                    date_center.append(t[iii_old] + (t[iii+1]-t[iii_old])/2)
                                    for k in range(1,Noverlap+1):
                                         i1 = iii_old+(iii-iii_old)*k//(Noverlap+1)
                                         i2 = iii+(iii-iii_old)*k//(Noverlap+1)
                                         interval.append([i1,i2])
                                         date_center.append(t[i1]+(t[i2+1]-t[i1])/2)
                        iii_old = iii+1
            Ndays = 3 # nb of days on which the model is fitted
            interval_bis = []
            date_center_bis = []
            for i in range(0,len(interval),Ndays):
                 if i+Ndays-1 < len(interval):
                      interval_bis.append([interval[i][0],interval[i+Ndays-1][1]])
                      date_center_bis.append(date_center[i]+(date_center[i+Ndays-1]-date_center[i])/2)
                      for k in range(1,Noverlap+1):
                           iii_old = interval[i][0]
                           iii = interval[i+Ndays-1][1]
                           i1 = iii_old+(iii-iii_old)*k//(Noverlap+1)
                           i2 = iii+(iii-iii_old)*k//(Noverlap+1)
                           if i2+1 < len(t)-1: 
                                interval_bis.append([i1,i2])
                                date_center_bis.append(t[i1]+(t[i2+1]-t[i1])/2)
            date_center= date_center_bis
            interval = interval_bis
            sol = []
            i=0
            for inter in interval:
                        P_temp = P[inter[0]:inter[1]]
                        ts_temp = t_sidereal[inter[0]:inter[1]]
                        model_temp = np.polyval(p,ts_temp)
                        model_temp = (model_temp - np.min(model_temp))/(np.max(model_temp)-np.min(model_temp)) # normalized to amplitude 1 between 0 and 1
                        model_temp = (max_gal-min_gal)*model_temp+min_gal # normalized to provided galactic min-max temperatures between min_gal and max_gal

                        #model_temp = model_temp - model_mean
                        if no_offset:
                             model = np.ones((n+1,len(model_temp)))
                             for kk in range(n+1):
                                  model[kk,:] = model_temp*ts_temp**kk
                        else:
                             model = np.ones((n+2,len(model_temp)))
                             for kk in range(n+1):
                                  model[1+kk,:] = model_temp*ts_temp**kk

                        #plotfig = True
                        #pdb.set_trace()
                        #if (date_center[i].day == 19 or date_center[i].day == 20) and date_center[i].month==2:
                        #            plotfig = True
                        aa = modelfit_recurs(ts_temp,P_temp,model,plotfig=False)
                        if no_offset:
                             test_gain = aa[0]
                             aa_temp = np.zeros(len(aa)+1)
                             aa_temp[1:len(aa)+1] = aa
                             aa = aa_temp.copy()
                             model_bis = np.zeros((model.shape[0]+1,model.shape[1]))
                             model_bis[1:,:] = model
                             model = model_bis.copy()
                        else:
                             test_gain = aa[1]

                        if test_gain>0: # positive gain : OK
                                    sol.append(aa)
                                    optimal_model = np.dot(sol[-1],model)

                                    Pcorr[inter[0]:inter[1]] = ((P_temp - np.polyval(p,ts_temp)-408.)+np.mean(np.polyval(p,ts_temp))) #/np.median(sol[:][1])
                                    Pcorr2[inter[0]:inter[1]] = (((P_temp-sol[-1][0])/sol[-1][1]-model_temp)+np.mean(model_temp))*sol[0][1]
                                    #Pcorr3[inter[0]:inter[1]] = ((P_temp-sol[-1][0])/sol[-1][1]-model_temp)                                    
                                    gain_temp = 0
                                    for kk in range(0,n+1):
                                         gain_temp = gain_temp + sol[-1][kk+1]*ts_temp**kk
                                    Pcorr3[inter[0]:inter[1]] = ((P_temp-sol[-1][0])/gain_temp-model_temp)                                    
 
                                    aa_old = aa
                        else: # negative gain => nan => take the last previous non nan solution
                                    if i != 0:
                                       sol.append(np.array([np.nan]*(n+2)))
                                       Pcorr[inter[0]:inter[1]] = ((P_temp - np.polyval(p,ts_temp)-408.)+np.mean(np.polyval(p,ts_temp))) #/np.median(sol[:][1])
                                       Pcorr2[inter[0]:inter[1]] = (((P_temp-aa_old[0])/aa_old[1]-model_temp)+np.mean(model_temp))*aa_old[1]
#                                       Pcorr3[inter[0]:inter[1]] = ((P_temp-aa_old[0])/aa_old[1])-model_temp)                                                
                                       gain_temp = 0
                                       for kk in range(0,n+1):
                                          gain_temp = gain_temp + sol[-1][kk+1]*ts_temp**kk
                                          Pcorr3[inter[0]:inter[1]] = ((P_temp-aa_old[0])/gain_temp)-model_temp                                                
                                    else:
                                       continue
                        #print(sol[-1][0]/sol[-1][1])
                        #pdb.set_trace()
                        # sol.append(np.dot(np.linalg.pinv(model.T),P_temp))
                        
                        
                        # figure()
                        # plot(ts_temp,P_temp,'.',label = "Signal original")
                        # plot(ts_temp,optimal_model,'r.',label = "Modele optimal")
                        # legend()
                        # figure()
                        # plot(ts_temp,(P_temp-sol[-1][0])/sol[-1][1],'.')
                        # pdb.set_trace()
                        i+=1
            #pudb.set_trace()
            sol = np.array(sol)
            ii = find(~np.isnan(sol[:,0]))
            tt = np.array(date_center)[ii]
            date_center_sidereal = to_sidereal(date_center)
            tts = np.array(date_center_sidereal)[ii]
#            Gain_ua_T = 1/(sol[ii,1])
            gain_temp = np.zeros(len(tts))
            for kk in range(0,n+1):
                 gain_temp = gain_temp + sol[ii,kk+1]*tts**(kk)
            Gain_ua_T = 1/gain_temp
            Tsys = sol[ii,0]/gain_temp #sol[ii,1]
            
            #for i in range(len(tt)):
            #            tt[i] = t[i].timestamp()
            
            #pTsys = polyfit_recurs(np.array(tt)[ii],Tsys[ii],plotfig=True)
            #pGain_ua_T = polyfit_recurs(np.array(tt)[ii],Gain_ua_T[ii],plotfig=True)
            return Pcorr,Pcorr2,Pcorr3,sol,tt,Tsys,Gain_ua_T,p





def get_lightnings(date0,date1):

            date0 = to_datetime(date0)
            date1 = to_datetime(date1)            
	    # if type(date0) != datetime.datetime:
            # 	        date0 = datetime.datetime(date0[0],date0[1],date0[2],date0[3],date0[4],date0[5])
	    # if type(date1) != datetime.datetime:
            # 	        date1 = datetime.datetime(date1[0],date1[1],date1[2],date1[3],date1[4],date1[5])
            
	    # data format : http://en.blitzortung.org/compendium.php
            data=[]
            date0_str = datetime.datetime.strftime(date0,"%d%m%y%H%M%S")
            date1_str = datetime.datetime.strftime(date1,"%d%m%y%H%M%S")
            url = 'http://193.55.144.222:8080/strikes/start/'+date0_str+'/end/'+date1_str
            #	url = 'http://193.55.144.222:8080/strikes/start/200503132500/end/200503132655'
            #	r = requests.get(url)
            #	print url.json()
            print("> loading lightnings infos ...")
            response = urllib.request.urlopen(url)
            #pdb.set_trace()
            json_list = response.read().split('\n')[:-1]
            #data_temp=json.loads(json.dumps(json_list)) #json.loads(json.dumps(json_list))
            #return data_temp
            for json_str in json_list:
                        data.append(json.loads(json_str))

            return data









####################################################################################################################
###################################### Simu Antenne et Ciel ########################################################
####################################################################################################################


def read_healpix_gain(freq,pol='NW',filename="NenuFAR_Ant_Hpx.fits",nside=64):
            # Correspondance between polar/freq and field index in FITS
            gain_freqs = np.arange(10, 90, 10)*1e6
            gain_freqs_int = np.arange(10, 90, 10, dtype=int)
            count = 0
            cols = {}
            for p in ['NE', 'NW']: #['NW', 'NE']:
                        for f in gain_freqs_int:
                                    cols['{}_{}'.format(p, f)] = count
                                    count += 1

            if freq < 10e6:
                        raise ValueError('No antenna model < 10 MHz.')
            elif freq > 80e6:
                        print('NenuFAR antenna response is extrapolated > 80 MHz.')            


                        # Will fit a polynomial along the high end of frequencies
                        freq_to_fit = np.arange(40, 90, 10, dtype=int)*1e6
                        freq_to_fit_int = np.arange(40, 90, 10, dtype=int)
                        gains = np.zeros((freq_to_fit.size, healpy.nside2npix(64)))
                        # Construct the gain map (freqs, npix)
                        for i, f in enumerate(freq_to_fit_int):
                                    gains[i, :] = healpy.read_map(
                                                filename=filename,
                                                hdu=1,
                                                field=cols['{}_{}'.format(pol, f)],
                                                verbose=False,
                                                memmap=True,
                                                dtype=float
                                    )
                        # Get the polynomial coefficients
                        coeffs = np.polyfit(freq_to_fit, gains, 3)
                        def poly(x, coeffs):
                             """ Retrieve the polynomial from coefficients
                             """
                             na = np.newaxis
                             order = coeffs.shape[0]
                             poly = np.zeros((x.size, coeffs.shape[1]))
                             for deg in range(order):
                                  poly += (x**deg)[:, na] * coeffs[order-deg-1, :][na, :]
                             return poly
                        gain = poly(np.array([freq]), coeffs).ravel()
                        gain = np.polyval(coeffs,np.array([freq]))
            else:
        
                        # Get Low and High ant gain bounding freq
                        f_low = gain_freqs[gain_freqs <= freq].max()
                        f_high = gain_freqs[gain_freqs >= freq].min()
                        gain_low = healpy.read_map(
                                    filename=filename,
                                    hdu=1,
                                    field=cols['{}_{}'.format(pol, int(f_low/1e6))],
                                    verbose=False,
                                    memmap=True,
                                    dtype=float
                        )
                        gain_high = healpy.read_map(
                                    filename=filename,
                                    hdu=1,
                                    field=cols['{}_{}'.format(pol, int(f_high/1e6))],
                                    verbose=False,
                                    memmap=True,
                                    dtype=float
                        )
                        # Make interpolation
                        if f_low != f_high:
                                    gain = gain_low * (f_high - freq)/10.e6 + gain_high * (freq - f_low)/10.e6
                        else:
                                    gain = gain_low


            # rotate for max gain at zenith (default:north on horizon in the file it seems)
            #rot = healpy.Rotator(deg=True,rot=[0, 90],inv=False)
            #gain=rot.rotate_map_alms(gain)
            # Convert HEALPix map to required nside
            gain = healpy.ud_grade(gain, nside_out=nside)
            return gain #/ gain.max()




def get_nenufar_ant_gain(az,el,freq):
            """
            renvoie le gain (en lineaire) pour une direction (el,az) en degres (az = azimuth, el=0 a l'horizon) pour la frequence freq en Hz.
            """

            if np.isscalar(az):
                 az = [az]
            if np.isscalar(el):
                 el = [el]
            if type(az)==list:
                 az = np.array(az)
            if type(el)==list:
                 el = np.array(el)

            gain_out = []
            el = (90.-el)/180*np.pi
            az = az/180*np.pi
            gain_map = read_healpix_gain(freq,nside=64,pol='NW')
            for azz in az:
                 for ell in el:
                      ipix = healpy.ang2pix(64,ell,azz) #,lonlat=True)
#                      ipix = healpy.ang2pix(64,azz,ell) #,lonlat=True)
                      gain_out.append(gain_map[ipix])

            return np.array(gain_out).reshape(len(el),len(az))



def get_gsm(freq,date, nside=64,lon=2.1147, lat=47.2247, h=23.):
            """
            """

            date = to_datetime(date)
            ov = pygsm.GSMObserver()
            ov.lon = str(lon)
            ov.lat = str(lat)
            ov.elev = h
            ov.date = date
            gsmap = ov.generate(freq/1e6)
#            gsm = pygsm.GlobalSkyModel(freq_unit='Hz')
#            gsmap = gsm.generate(freq)
            # Convert HEALPix map to required nside
            #pdb.set_trace()
            gsmap = healpy.ud_grade(gsmap, nside) #,order_in='NESTED') #,order_out='RING') #'RING') #

            return gsmap


def get_gsm_daily_power(freq):
        gain = read_healpix_gain(freq,pol='NW')
        N = len(gain)
        fov = 60. # half a fov actually
        dOmega = 4*np.pi/N
        Omega = fov/180*4*np.pi
        pow = []
        #ii = healpy.query_disc(64,[0,0,1],Omega)	
        ii = range(len(gain))
        Omega = 4*np.pi
        t_sidereal = []
        for h in range(24):
            date = (2020,4,1,h,0,0)
            gsmap = get_gsm(freq,date)
            pow.append(np.sum(gsmap[ii]*gain[ii]*dOmega)/Omega)
            t_sidereal.append(to_sidereal([to_datetime(date)]))

        ii = np.argsort(np.squeeze(np.array(t_sidereal)))
        return np.squeeze(np.array(pow)[ii]),np.squeeze(np.array(t_sidereal)[ii])



def get_gsm_power_short(freq):
        gain = read_healpix_gain(freq,pol='NW')
        N = len(gain)
        fov = 60. # half a fov actually
        dOmega = 4*np.pi/N
        Omega = fov/180*4*np.pi
        pow = []
        #ii = healpy.query_disc(64,[0,0,1],Omega)	
        ii = range(len(gain))
        Omega = 4*np.pi
        t_sidereal = []
        for m in range(60):
            date = (2020,4,1,0,m,0)
            gsmap = get_gsm(freq,date)
            pow.append(np.sum(gsmap[ii]*gain[ii]*dOmega)/Omega)
            t_sidereal.append(to_sidereal([to_datetime(date)]))

        ii = np.argsort(np.squeeze(np.array(t_sidereal)))
        return np.squeeze(np.array(pow)[ii]),np.squeeze(np.array(t_sidereal)[ii])


def get_gsm_sidereal_power_1D(freq,nx=11,ny=11,date0=(2020,1,1,0,0,0)):
        """
        Outputs a polynom fitting sidereal power fluctuation for a given frequency
        nx : order of polynom along time dimension
        ny : order of polynom along frequency dimension
        """

        date0 = to_datetime(date0)
        gain = read_healpix_gain(freq,pol='NW')
        N = len(gain)
        fov = 60. # half a fov actually
        dOmega = 4*np.pi/N
        Omega = fov/180*4*np.pi
        pow = []
        #ii = healpy.query_disc(64,[0,0,1],Omega)	
        ii = range(len(gain))
        Omega = 4*np.pi

        t_sidereal = []
        ts_offset = 0
        ts_old = 0
        i = 0
        first_ts = 0
        ts = 0
        while ts+ts_offset < first_ts + 2*np.pi: #i<96+12*4: # every 15 min => 96s sample for 24 hours + 12 hours margin
            date = date0 + i*datetime.timedelta(seconds=60*15)
            gsmap = get_gsm(freq,date)
            pow.append(np.sum(gsmap[ii]*gain[ii]*dOmega)/Omega)
            ts = to_sidereal([to_datetime(date)])
            if i != 0:
                 if ts-ts_old < 0:
                      ts_offset = ts_offset+2*np.pi
            if i ==0:
                 first_ts = ts
            ts_old = ts
            t_sidereal.append(ts+ts_offset)
            i+=1

        #ii = np.argsort(np.squeeze(np.array(t_sidereal)))
        t_sidereal = np.squeeze(np.array(t_sidereal))-2*np.pi #[ii]
        pow = np.squeeze(np.array(pow)) #[ii]

        ii = np.argsort(t_sidereal)
        t_sidereal= t_sidereal[ii]
        pow = pow[ii]

        ii1 = find(t_sidereal>=0)
        ii2 = find(t_sidereal<0)

        missing_pow = pow[ii1[0]]-(t_sidereal[ii1[0]]-0)*(pow[ii1[0]]-pow[ii2[-1]])/(t_sidereal[ii1[0]]-t_sidereal[ii2[-1]])

        pow1 = pow[ii1].reshape(1,len(ii1))
        pow2 = pow[ii2].reshape(1,len(ii2))


        pow = np.squeeze(np.concatenate((pow1,pow2),axis=1))
        t_sidereal = np.squeeze(np.concatenate((t_sidereal[ii1].reshape(1,len(ii1)),(t_sidereal[ii2]+2*np.pi).reshape(1,len(ii2))),axis=1))

        pow = np.concatenate((np.ones((1,1))*missing_pow,pow.reshape(1,len(pow)),np.ones((1,1))*missing_pow),axis=1)
        t_sidereal = np.concatenate((0*np.ones((1,1)),t_sidereal.reshape(1,len(t_sidereal)),2*np.pi*np.ones((1,1))),axis=1)

        pow = np.squeeze(pow)
        t_sidereal = np.squeeze(t_sidereal)

        ii = np.argsort(t_sidereal)
        t_sidereal= t_sidereal[ii]
        pow = pow[ii]


        #p = np.polyfit(t_sidereal,pow,nx)
        p = interpolate.CubicSpline(t_sidereal,pow,bc_type='periodic')

        return p, pow, t_sidereal


############## A completer/adapter ###################
def spline2D():
        aa=scipy.interpolate.interp2d(ts, freqs, POW, kind='cubic', copy=True, bounds_error=False, fill_value=nan)
        ts2=linspace(ts[0],ts[-1],99)
        freqs2=linspace(freqs[0],freqs[-1],91)
        M =aa(ts2,freqs2)



# example from calibrator: export fichier cal
def export_rmhdf(self,type_data):        
        #creation du fichier RMHDF                               
        #self.nom_fichier_rmhdf=str(self.ID)+"_CAL.hdf5"        
        self.fname2, _filter = QtWidgets.QFileDialog.getSaveFileName(self, 'sauvegarde fichier cal hfd5', self.nom_fichier_export+".hdf5", os.getenv('HOME'), 'HDF5(*.hdf5)')        
        self.fichierHDF5 = h5py.File(self.fname2, 'w')
                
        # Creation de l'AcqRec
        acq_rec = self.fichierHDF5.create_group("AcqRec_000")
                
        # Creation du niveau Pointage
        pointing = acq_rec.create_group("Pointing_000")
       
        # Création du niveau Polar 1
        #polar = pointing.create_group("Polar_"+str(self.polar))
        polar0 = pointing.create_group("Polar_0")
        
        if type_data==0:  #mode Grec
            data1= self.Grec1
            data2= self.Grec2
            
            unit=self.Grec_unit
            
        if type_data==1:  #mode Gsys
            data1= self.Gsys1
            data2= self.Gsys2
            unit=self.Gsys_unit
                
                
        # Creation du dataset
        cal0 = polar0.create_group("Cal")
        scan = cal0.create_dataset('scan_' + "".zfill(6), data=data1) 
        scan.attrs['FFTchannel0'] = ""
        scan.attrs['TimeStamp'] = ""
        scan.attrs['nbins'] =  str(self.nbr_points)        
        
        # Création du niveau Polar2
        #polar = pointing.create_group("Polar_"+str(self.polar))
        polar1 = pointing.create_group("Polar_1")
                
        # Creation du dataset
        cal1 = polar1.create_group("Cal")
        scan = cal1.create_dataset('scan_' + "".zfill(6), data=data2)
        scan.attrs['FFTchannel0'] = ""
        scan.attrs['TimeStamp'] = ""
        scan.attrs['nbins'] =  str(self.nbr_points)
       
        ## Stockage des attributs du fichier HDF5
        ## Niveau 0 (root)
        self.fichierHDF5.attrs['AntennaCoordinates'] = self.coordonnee
        self.fichierHDF5.attrs['AntennaGain'] = self.antenna_gain
        self.fichierHDF5.attrs['AntennaHeight'] = self.antenna_height
        self.fichierHDF5.attrs['AntennaType'] = self.antenna_type

        self.fichierHDF5.attrs['DataFormat'] = self.data_format
        self.fichierHDF5.attrs['FileId'] = "CAL"+self.fileID    
        self.fichierHDF5.attrs['ObsIdCfg'] = str(self.ID)
        self.fichierHDF5.attrs['ObsStart'] = self.start1
        self.fichierHDF5.attrs['ObsStop'] = self.stop1
        self.fichierHDF5.attrs['ReceiverType'] = self.type_rec
        self.fichierHDF5.attrs['Scanning'] = self.scan_mode
        self.fichierHDF5.attrs['StationCode'] = self.station_code
        self.fichierHDF5.attrs['StationName'] = self.antenne
        self.fichierHDF5.attrs['Theme'] = self.theme 
        self.fichierHDF5.attrs['Title'] = self.title
        
        
    
        #self.CfgID        
              

        ## Niveau 1A (AcqRec_xxx)                                       
        acq_rec.attrs['BinNum'] = self.nbr_points
        acq_rec.attrs['Fa3'] = self.fa
        acq_rec.attrs['Fb3'] = self.fb 
        acq_rec.attrs['Fcenter'] = self.freq_centrale
        acq_rec.attrs['Index'] = 0
        acq_rec.attrs['MeasType'] = self.meas_type
        acq_rec.attrs['Name'] = self.nom_acq
        acq_rec.attrs['RBW'] = self.resolution
        acq_rec.attrs['ScanDuration'] = self.scan_dur
        acq_rec.attrs['Span'] = self.span
        acq_rec.attrs['SweepNum'] = self.sweepnum
        acq_rec.attrs['VBW'] = ""
        
 
        ## Niveau 2A (Pointing_xxx)               
        pointing.attrs['Azimuth'] = ""
        pointing.attrs['Site'] = ""

        ## Niveau 3A (Polar_x)        
        polar0.attrs['Polarization'] = self.var_liste_polar2[0]
        
        if len(self.var_liste_polar2)>1:
            polar1.attrs['Polarization'] = self.var_liste_polar2[1]
        else:
            
            polar1.attrs['Polarization'] =""

        ## Niveau 4A (Cal)            
        cal0.attrs['ChanInteTime'] = ""
        cal0.attrs['DataStatus'] = "raw"
        cal0.attrs['Detector'] = ""
        cal0.attrs['MeasurementAccuracy'] = ""

        cal0.attrs['NumScan'] = 1
        cal0.attrs['ScanMode'] = "specific"
        cal0.attrs['Unit'] = str(unit)

        cal1.attrs['ChanInteTime'] = ""
        cal1.attrs['DataStatus'] = "raw"
        cal1.attrs['Detector'] = ""
        cal1.attrs['MeasurementAccuracy'] = ""
        
        cal1.attrs['NumScan'] = 1
        cal1.attrs['ScanMode'] = "specific"
        cal1.attrs['Unit'] = str(unit)        
 
        ## Niveau 5A (Cal)                       
        #scan.attrs['TimeStamp'] = ""
        #scan.attrs['Percent'] = ""
        #scan.attrs['FFTchannel0'] = ""        
        
        self.fichierHDF5.close()
        
        msg = QMessageBox()
        msg.setWindowTitle(" ")
        msg.setIcon(QMessageBox.Information)
        msg.setText("Fichier "+ str(self.nom_fichier_rmhdf)+" créé")        
        msg.exec_()



#np.squeeze(np.array(pow)[ii]),np.squeeze(np.array(t_sidereal)[ii])


# minmax temperature (K) @40MHz according to gsm
# 13125.7716611 6791.64819438

# read nenufar healpix antenna file:
# aa=healpy.read_map('./NenuFAR_Ant_Hpx.fits',field=1)
# field = 0 temperature ? field = 1 correspond a Q, field = 2 U
# imshow(aa.reshape(128,128*3),interpolation='None')
# parametre Nside du healpix = 64, npix = 49152 = 2**14*3
#
# read coords:
# healpy.pix2ang(nside,i_pix)
# http://193.55.144.222:8080/strikes/start/200503132500/end/200503132655
        
# stdd = wavelets.stdMAD(10*log10(P))
# avgg = np.median(10*log10(P))

# print "> Fond moyen : "+str(avgg)+" dB"
# print "> std : "+str(stdd)+" dB"

# seuil = avgg + 5*stdd

# ii = find(10*log10(P)>seuil)

# print "> N events found"
# candidate_files = set(iFiles[ii])
# N = len(candidate_files)
# print "\> in "+str(N)+" Files:"
# k=0
# for i in list(candidate_files):
# 	print matches[int(i)]
# 	k+=1

# t1 = time.time()

# print "%.1f"%(t1-t0)+" s elapsed"




        
##############################################        
############ Historique ######################
##############################################




# attributes (metadata) in hdf5 file

# level 0: root
#[u'AntennaHeight',
# u'Title',
# u'AntennaCoordinates',
# u'AntennaGain',
# u'FileId',
# u'^AntennaType',
# u'DataFormat',
# u'ReceiverType',
# u'StationCode',
# u'StationName',
# u'Scanning',
# u'Theme',
# u'ObsIdCfg',
# u'ObsStart',
# u'ObsStop']


# level 1: pour chaque acq_rec
#[u'Fcenter',
# u'ScanDuration',
# u'Index',
# u'SweepNum',
# u'Name',
# u'MeasType',
# u'BinNum',
# u'Span',
# u'RBW',
# u'VBW',
# u'Fa3',
# u'Fb3']


# level 2: pour chaque pointing
#[u'Azimuth',
#u'Site']

# level 3: pour chaque polar
# [u'Polarization']

# level 4: pour chaque acq
#[u'ChanInteTime',
# u'ScanMode',
# u'MeasurementAccuracy',
# u'DataStatus',
# u'Detector',
# u'Unit',
# u'NumScan']


# level 5: pour chaque scan
#[u'TimeStamp',
# u'FFTchannel0',
# u'nbins']











#########################################################################################################################################################################

obsname = "20190310_0302_NSA20190310-01_0587_Mon10-100M_Ro14"

def read_NSA_hdf5_xml(obsname):
        """
        read NSA hdf5 data file and get metadata in associated xml file
        """
        dirname = '/databf/nsa/obs/'+obsname
        filename = "_".join(obsname.split('_')[2:])
        filepath = dirname+'/'+filename+".hdf5"
        xml_name = dirname+'/dataset.xml'

        #### read metadata #########

        ### in xml file
        print("##### Repertoire : "+dirname+" ########")
        fid=open(xml_name,'r')
        aa = fid.readlines()
        dic=xmltodict.parse("\n".join(aa))
        #dic=xmltodict.parse(fid)
        fid.close()
        # dates
        date_depart = dic['Observation']['@depart']
        date_fin = dic['Observation']['@fin']

        print("\n> Date depart : "+date_depart)
        print("> Date fin : "+date_fin)

        # pointing
        az_consigne = float(dic['Observation']['Pointage']['@azimut_consigne'])
        el_consign = float(dic['Observation']['Pointage']['@site_consigne'])
        az_reel = float(dic['Observation']['Pointage']['@azimut_reel'])
        el_reel = float(dic['Observation']['Pointage']['@site_reel'])

        print("\naz_reel : "+str(az_reel))
        print("el_reel : "+str(el_reel))


        # receiver
        if type(dic['Observation']['Pointage']['Configuration_Roach']) == list:
                Nacq = len(dic['Observation']['Pointage']['Configuration_Roach'])
        else:
                Nacq = 1
        timestamp0 = np.zeros((Nacq),dtype='datetime64[us]')
        timestamp1 = np.zeros((Nacq),dtype='datetime64[us]')
        Freq_centrale = np.zeros(Nacq)
        Tint = np.zeros(Nacq)
        Nspec_int = np.zeros(Nacq)
        nspectres = np.zeros(Nacq)
        duree_msec = np.zeros(Nacq)
        timestamp = np.zeros(Nacq,dtype='datetime64[us]') # date de changement de config de l'antenne
        nrepet = np.zeros(Nacq)
        DAB_BF = np.zeros(Nacq,dtype=bool)
        DAB_HF = np.zeros(Nacq,dtype=bool)
        df_BF = np.zeros(Nacq)
        df_HF = np.zeros(Nacq)
        Nchan_BF = np.zeros(Nacq)
        Nchan_HF = np.zeros(Nacq)
        RFband_BF = np.zeros(Nacq)
        RFband_HF = np.zeros(Nacq)

        ##########################
        ## BEGIN LOOP on acqrec ##
        ##########################

        for i in range(Nacq):
                # config roach
        #['@timestamp_1ere_trame',
        # '@timestamp_derniere_trame',
         #'ADC_0_BF_Conf',
         #'ADC_0_HF_Conf',
        # 'duree_Integration',
        # 'nspec_integration',
        # "'RapportAcq']
                timestamp0[i] = datetime.datetime.strptime(dic['Observation']['Pointage']['Configuration_Roach'][i][u'@timestamp_1ere_trame'],"%d/%m/%Y %H:%M:%S.%f")
                timestamp1[i] = datetime.datetime.strptime(dic['Observation']['Pointage']['Configuration_Roach'][i][u'@timestamp_derniere_trame'],"%d/%m/%Y %H:%M:%S.%f")
        #dic['Observation']['Pointage']['Configuration_Roach'][i]['@timestamp_derniere_trame']

                Tint[i] = float(dic['Observation']['Pointage']['Configuration_Roach'][i]['duree_Integration']['#text'])
                Tint_units = dic['Observation']['Pointage']['Configuration_Roach'][i]['duree_Integration']['@unites']
                if Tint_units == 'msec':
                        Tint[i] = Tint[i]*1e3 # s
                else:
                        sys.exit("!!! Unknown Tint units: "+Tint_units+" !!!!")
                Nspec_int[i] = dic['Observation']['Pointage']['Configuration_Roach'][i]['nspec_integration'] # C'est quoi ?? c pas nspectres en tout cas

                BF_conf = dic['Observation']['Pointage']['Configuration_Roach'][i]['ADC_0_BF_Conf']['@desc']
        # conf inutilise en HF??	HF_conf = dic['Observation']['Pointage']['Configuration_Roach'][i]['ADC_0_HF_Conf']['@desc']

                df_BF[i] = float(BF_conf.split(' ')[1].split('=')[-1])*1e3 # Hz (if kHz in the description!!)
                #df_HF[i] = float(HF_conf.split(' ')[1].split('=')[-1])*1e3 # Hz (if kHz in the description!!)
                Nchan_BF[i] = int(BF_conf.split(' ')[3].split('=')[-1])
                #Nchan_HF[i] = int(HF_conf.split(' ')[3].split('=')[-1])
                RFband_BF[i] = float(BF_conf.split(' ')[4].split('=')[-1].split('M')[0])*1e6 # MHz, really dirty, works only if 'MHz' in description !!
                #RFband_HF[i] = float(HF_conf.split(' ')[4].split('=')[-1].split('M')[0]) # MHz, really dirty, works only if 'MHz' in description !!

        #dic['Observation']['Pointage']['Configuration_Roach'][i]['ADC_0_BF_Conf']
        #Out[39]: OrderedDict([(u'@index', u'9'), (u'@desc', u'Fech=400MHz Res=97.6 KHz Ncanaux=2048 BandeRF=200Mhz Mode=power Firmware=nsa_200Mhz_adc0_pow2048.bof')])

                # config acqrec
        #[u'@index',
        # u'@timestamp',
        # u'@nom',
        # u'@description',
        # u'@duree_msec',
        # u'@nrepet',
        # u'@duree_entre_repet',
        # u'@nspectres',
        # '#text']
        #dic['Observation']['Pointage']['Configuration_AcqRec'][i].keys()

                nspectres[i] = dic['Observation']['Pointage']['Configuration_AcqRec'][i]['@nspectres'] # nb spectra, 2 polars added
                duree_msec[i] = dic['Observation']['Pointage']['Configuration_AcqRec'][i]['@duree_msec']
                timestamp[i] = datetime.datetime.strptime(dic['Observation']['Pointage']['Configuration_AcqRec'][i]['@timestamp'],"%d/%m/%Y %H:%M:%S.%f") # comment ca se compare a timestamp0 ??

                nrepet[i] = dic['Observation']['Pointage']['Configuration_AcqRec'][i]['@nrepet'] # c'est quoi ??


                # config recepteur
        #[u'@timestamp',
        # u'Freq_Centrale',
        # u'Voie_BF',
        # u'Voie_HF',
        # u'Commutateur_AnalyseurSpectres',
        # u'OL1_freq',
        # u'Down_Converter',
        # u'Commutateur_Entree_Video']

                Freq_centrale[i] =  float(dic['Observation']['Pointage']['Configuration_Recepteur'][i]['Freq_Centrale']['#text'])

                Freq_units = dic['Observation']['Pointage']['Configuration_Recepteur'][i]['Freq_Centrale'][u'@Unit\xe9s']

                if Freq_units == 'MHz':
                        Freq_centrale[i] = Freq_centrale[i]*1e6 # Hz
                else:
                        sys.exit("!!! Unknown Freq_centrale units: "+Freq_units+" !!!!")

                DAB_BF[i] = dic['Observation']['Pointage']['Configuration_Recepteur'][i]['Voie_BF']['DAB']=='ON' # True = DAB ON
                DAB_HF[i] = dic['Observation']['Pointage']['Configuration_Recepteur'][i]['Voie_HF']['DAB']=='ON' # True = DAB ON

        ##########################
        ## END LOOP on acqrec ##
        ##########################


        # in hdf5 file
        f=h5py.File(filepath,'r')

        #f.attrs['AntennaGain']
        #f.attrs['AntennaHeight']
        #f.attrs['AntennaCoordinates']
        #f.attrs['AntennaType']

        #Nf = len(f['AcqRec_000/Pointing_000/Polar_0/Stat/scan_000000']) # dirty way for getting nb of frequencies

        DATA0 = np.zeros((2,nspectres[0],Nchan_BF[0]+Nchan_HF[0])) #npolar x nspectres x nchannel
        DATA1 = np.zeros((2,nspectres[1],Nchan_BF[1]+Nchan_HF[1]))
        DATA2 = np.zeros((2,nspectres[2],Nchan_BF[2]+Nchan_HF[2]))
        DATA3 = np.zeros((2,nspectres[3],Nchan_BF[3]+Nchan_HF[3]))

        i_acqrec = 0
        i_pointing = 0
        i_polar = 0
        i_acq=0
        for acqrec_id, acqrec in f.items(): # acqrec_id = 0..Nacq-1
                for key_pointing in acqrec.keys():
                        for key_polar in acqrec[key_pointing].keys():
                                for key_acq in acqrec[key_pointing][key_polar]['Acq'].keys():
                                        print(acqrec_id, key_pointing, key_polar, key_acq)
                                        if i_acqrec == 0:
                                                DATA0[i_polar,i_acq,:] = acqrec[key_pointing][key_polar]['Acq'][key_acq]
                                        elif i_acqrec == 1:
                                                DATA1[i_polar,i_acq,:] = acqrec[key_pointing][key_polar]['Acq'][key_acq]
                                        elif i_acqrec == 2:
                                                DATA2[i_polar,i_acq,:] = acqrec[key_pointing][key_polar]['Acq'][key_acq]
                                        elif i_acqrec == 1:
                                                DATA3[i_polar,i_acq,:] = acqrec[key_pointing][key_polar]['Acq'][key_acq]
                                        #semilogy(acqrec[key_pointing][key_polar]['Acq'][key_acq])
                                        #raw_input()
                                        i_acq+=1
                                i_acq = 0
                                i_polar+=1
                        i_polar = 0
                        i_pointing+=1
                i_pointing = 0
                i_acqrec+=1

        return DATA0,DATA1,DATA2,DATA3
